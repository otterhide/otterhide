{
  description = "Simulate and explore a history of a town";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        project-name = "otterhide";
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        buildInputs = with pkgs; [
          openssl
          alsa-lib
          udev
          libxkbcommon
          vulkan-loader
          wayland
          libffi
        ];
        nativeBuildInputs = with pkgs; [
          (rust-bin.fromRustupToolchainFile ./rust-toolchain.toml)
          gcc
          pkg-config
          gnumake
          findutils
          gzip
          lld
          trunk
          wasm-bindgen-cli
          brotli
          rustPlatform.bindgenHook
        ];
      in
      {
        devShell = pkgs.mkShell {
          inherit buildInputs nativeBuildInputs;
          name = "${project-name}-develpoment-shell";
          packages = [
            pkgs.rust-analyzer
            pkgs.miniserve
            pkgs.jq
            pkgs.watchexec
            pkgs.f3d
            pkgs.rlwrap
          ];
          project_name = project-name; # Expose as an environment variable for make
          LD_LIBRARY_PATH = "$LD_LIBRARY_PATH:${ with pkgs; lib.makeLibraryPath buildInputs }";
          XCURSOR_THEME = "Adwaita"; # Workaround for https://github.com/bevyengine/bevy/issues/4768
        };
        defaultPackage = pkgs.rustPlatform.buildRustPackage {
          # TODO: Consider using naersk here
          name = project-name;
          version = "1.0.0";
          src = ./.;
          cargoLock = {
            lockFile = ./Cargo.lock;
            outputHashes = {
              "bevy-inspector-egui-0.23.4" = "sha256-hbtkiaFpONeTnQtd2xZM2rIl9A5D4WsatXXtQFgi3Z8=";
            };
          };
          postFixup = ''
            # See https://discourse.nixos.org/t/rust-bevy-vulkan-loader-and-ld-library-path-variable/25282/2
            patchelf --add-rpath ${ pkgs.vulkan-loader }/lib $out/bin/*
          '';

          inherit buildInputs nativeBuildInputs;
        };
        packages.docker-image = pkgs.dockerTools.buildLayeredImage {
          name = "${project-name}-build-image";
          tag = "latest";
          created = "now";
          contents = buildInputs ++ nativeBuildInputs ++ [
            pkgs.bash
            pkgs.coreutils
            pkgs.git
            pkgs.httpie
            pkgs.rustup
            pkgs.cacert
          ];
          fakeRootCommands = ''
            mkdir --parents /tmp
          '';
          enableFakechroot = true;
          config.Cmd = [ "${ pkgs.bash }/bin/bash" ];
        };
      }
    );
}
