use bevy::prelude::*;
use bevy::utils::Uuid;
#[cfg(not(debug_assertions))]
use bevy_egui::EguiPlugin;
#[cfg(debug_assertions)]
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_mod_picking::DefaultPickingPlugins;
use otterhide::buildings::BuildingsPlugin;
use otterhide::camera::CameraPlugin;
use otterhide::configuration::{Configuration, ConfigurationPlugin};
use otterhide::date::DatePlugin;
use otterhide::districts::DistrictsPlugin;
use otterhide::ground::GroundPlugin;
use otterhide::heads_up_display::HudPlugin;
use otterhide::history::HistoryPlugin;
use otterhide::major_state::MajorStatePlugin;
use otterhide::parcels::ParcelsPlugin;
use otterhide::population::PopulationPlugin;
use otterhide::roads::RoadsPlugin;
use otterhide::searchlight::SearchlightPlugin;
use otterhide::selecting::SelectingPlugin;
use otterhide::simulation::SimulationLoadingPlugin;
use otterhide::sun::SunPlugin;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "Otterhide".into(),
                canvas: Some("#game-canvas".into()),
                mode: bevy::window::WindowMode::Fullscreen,
                ..default()
            }),
            ..default()
        }))
        .register_type::<Uuid>()
        .add_plugins(DefaultPickingPlugins)
        .add_plugins(ConfigurationPlugin)
        .add_plugins(MajorStatePlugin)
        .add_plugins(SimulationLoadingPlugin)
        .add_plugins(CameraPlugin)
        .add_plugins(GroundPlugin)
        .add_plugins(RoadsPlugin)
        .add_plugins(ParcelsPlugin)
        .add_plugins(DistrictsPlugin)
        .add_plugins(BuildingsPlugin)
        .add_plugins(DatePlugin)
        .add_plugins(SunPlugin)
        .add_plugins(HistoryPlugin)
        .add_plugins(DebugPlugin)
        .add_plugins(HudPlugin)
        .add_plugins(PopulationPlugin)
        .add_plugins(SelectingPlugin)
        .add_plugins(SearchlightPlugin)
        .add_systems(Startup, greet)
        .run()
}

fn greet() {
    info!("Let's build the city on rock and roll!")
}

struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        #[cfg(debug_assertions)]
        app.add_plugins(WorldInspectorPlugin::new());
        #[cfg(not(debug_assertions))]
        app.add_plugins(EguiPlugin);

        app.add_systems(Update, toggle_gizmos);
    }
}

fn toggle_gizmos(configuration: Res<Configuration>, mut gizmos: ResMut<GizmoConfigStore>) {
    let gizmo_config = gizmos.config_mut::<DefaultGizmoConfigGroup>().0;
    gizmo_config.enabled = configuration.gizmos;
}
