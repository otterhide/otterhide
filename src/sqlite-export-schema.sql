Drop table if exists district;
Create table district (
  name text
);

Drop table if exists building;
Create table building (
  id uuid primary key not null,
  address text not null,
  construction_year integer not null,
  construction_month integer not null check (construction_month between 1 and 12),
  longitude real not null,
  latitude real not null
);

Drop table if exists person;
Create table person (
  id uuid primary key not null,
  name text not null,
  sex text check (sex in ('♂', '♀')) not null,

  birth_year integer not null,
  birth_month integer not null check (birth_month between 1 and 12),

  death_year integer,
  death_month integer check (death_month between 1 and 12),

  -- Neither part of the death date can be null, unless both are
  check ((death_year is null) == (death_month is null))

  -- TODO: date_of_death
  -- TODO: mother
  -- TODO: father

);

Drop table if exists migration;
Create table migration (
  person uuid not null references person(id),
  arrival_year integer,
  arrival_month integer check (arrival_month between 1 and 12),
  departure_year integer,
  departure_month integer check (arrival_month between 1 and 12),

  -- Neither part of the date can be null, unless both are
  check ((arrival_year is null) == (arrival_month is null)),
  check ((departure_year is null) == (departure_month is null))
);

Drop table if exists residency;
Create table residency (
  person uuid not null references person(id),
  building uuid not null references building(id),
  from_year integer not null,
  from_month integer not null check (from_month between 1 and 12),
  until_year integer,
  until_month integer check (until_month between 1 and 12),

  -- Neither part of the leave date can be null, unless both are
  check ((until_year is null) == (until_month is null))
);
