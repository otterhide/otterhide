use bevy::math::{Vec2, Vec3};
use derive_more::{AsRef, From};
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::fmt::Display;
use std::marker::PhantomData;
use std::ops::{Add, AddAssign, Sub, SubAssign};

// TODO: Make a derive macro for Dimension
/// A generic dimension, like X, Y, Latitude, Longitude, Altitude, Time, etc.
pub trait Dimension {}
impl Dimension for Latitude {}
impl Dimension for Longitude {}

#[derive(Debug, Default, PartialEq, Eq, Hash, Clone, Copy, PartialOrd, Ord, From)]
pub struct Latitude;

#[derive(Debug, Default, PartialEq, Eq, Hash, Clone, Copy, PartialOrd, Ord, From)]
pub struct Longitude;

/// A coordinate in a specific dimension T
///
/// Coordinates are parameterized with a dimension, so they can't be confused.
///
/// For example, coordinates of the same dimension can be compared
///
/// ```
/// use otterhide::coordinates::*;
///
/// #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
/// struct X;
/// impl Dimension for X {}
///
/// #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
/// struct Y;
/// impl Dimension for Y {}
///
/// let x1 = Coordinate::<X>::new(-5);
/// let x2 = Coordinate::<X>::new(20);
/// assert!(x1 < x2);
/// ```
///
/// But this should not compile with a `mismatched types` error.
///
/// ``` compile_fail
/// # use otterhide::new_coordinates::*;
/// #
/// # #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
/// # struct X;
/// # impl Dimension for X {}
/// #
/// # #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
/// # struct Y;
/// # impl Dimension for Y {}
/// #
/// let x = Coordinate::<X>::new(-5);
/// let y = Coordinate::<Y>::new(-5);
/// assert_eq!(x, y);
/// ```
///
#[derive(
    Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Serialize, Deserialize,
)]
pub struct Coordinate<T>(i32, PhantomData<T>)
where
    T: Dimension + Clone + Ord + PartialOrd + Eq + PartialEq;

/// First element is longitude from west to east, second is latitude from north to south
// TODO: Specify Latitude and Longitude types for safety
#[derive(Debug, Default, PartialEq, Eq, Hash, Clone, Copy, AsRef, Serialize, Deserialize)]
pub struct Coordinates(Coordinate<Longitude>, Coordinate<Latitude>);

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum Direction {
    North = 0b0001,
    East = 0b0010,
    South = 0b0100,
    West = 0b1000,
}
impl Direction {
    pub(crate) fn opposite(&self) -> Direction {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East,
        }
    }
}

impl Coordinates {
    pub fn shift(&mut self, distance: i32, direction: &Direction) -> &mut Self {
        match direction {
            Direction::North => self.1 -= distance,
            Direction::East => self.0 += distance,
            Direction::South => self.1 += distance,
            Direction::West => self.0 -= distance,
        };
        self
    }

    pub fn longitude(&self) -> Coordinate<Longitude> {
        self.0
    }

    pub fn latitude(&self) -> Coordinate<Latitude> {
        self.1
    }

    pub fn new(longitude: Coordinate<Longitude>, latitude: Coordinate<Latitude>) -> Self {
        Self(longitude, latitude)
    }
}

impl From<&Coordinates> for Vec2 {
    fn from(coordinates: &Coordinates) -> Self {
        let x: i32 = coordinates.longitude().borrow().into();
        let y: i32 = coordinates.latitude().borrow().into();

        Self::new((x * 10) as f32, (y * 10) as f32)
    }
}

impl From<&Coordinates> for Vec3 {
    fn from(coordinates: &Coordinates) -> Self {
        let x: i32 = coordinates.longitude().borrow().into();
        let z: i32 = coordinates.latitude().borrow().into();

        Self::new((x * 10) as f32, 0.0, (z * 10) as f32)
    }
}

impl<T: Dimension + Clone + Ord> Coordinate<T> {
    pub fn new(value: i32) -> Self {
        Self(value, PhantomData)
    }

    pub fn raw_value(&self) -> i32 {
        self.0
    }

    pub fn range(&self, other: &Self) -> Vec<Self> {
        let start = self.min(other).raw_value();
        let end = self.max(other).raw_value();
        let values = start..=end;
        values.map(Self::new).collect()
    }

    /// Get a distance between two coordinates of the same dimension
    pub fn distance(&self, other: &Self) -> u32 {
        self.0.abs_diff(other.0)
    }
}

impl<T: Dimension + Clone + Ord> Add<i32> for Coordinate<T> {
    type Output = Self;
    fn add(self, rhs: i32) -> Self::Output {
        Self(self.0 + rhs, PhantomData)
    }
}

impl<T: Dimension + Clone + Ord> Sub<i32> for Coordinate<T> {
    type Output = Self;
    fn sub(self, rhs: i32) -> Self::Output {
        Self(self.0 - rhs, PhantomData)
    }
}

impl<T: Dimension + Clone + Ord> AddAssign<i32> for Coordinate<T> {
    fn add_assign(&mut self, rhs: i32) {
        self.0 += rhs;
    }
}

impl<T: Dimension + Clone + Ord> SubAssign<i32> for Coordinate<T> {
    fn sub_assign(&mut self, rhs: i32) {
        self.0 -= rhs;
    }
}

impl<T> From<&Coordinate<T>> for i32
where
    T: Dimension + Clone + Ord,
{
    fn from(value: &Coordinate<T>) -> Self {
        value.0
    }
}

impl Display for Coordinates {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Coordinates(Coordinate(x, ..), Coordinate(z, ..)) = self;
        write!(f, "({x}, {z})")
    }
}

impl<T> Display for Coordinate<T>
where
    T: Dimension + Clone + Ord,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Self(value, ..) = self;
        write!(f, "{value}")
    }
}

#[cfg(test)]
mod coordinate_tests {
    use super::*;

    // In this test we use different dimensions - X and Y
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
    struct X;
    impl Dimension for X {}

    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
    struct Y;
    impl Dimension for Y {}

    // So the coordinates is also different
    struct Coordinates {
        x: Coordinate<X>,
        y: Coordinate<Y>,
    }

    #[test]
    fn test_coordinate() {
        let x1 = Coordinate::<X>::new(10);
        let x2 = Coordinate::<X>::new(-20);
        let y1 = Coordinate::<Y>::new(10);
        let y2 = Coordinate::<Y>::new(-20);

        // You can compare coordinates of the same dimension
        assert!(x1 > x2);
        assert_ne!(y1, y2);
        assert_eq!(x1.distance(&x2), 30);
        assert_eq!(y2.distance(&y1), 30);

        // If you want to compare raw values, you can, but you got to be explicit!
        assert_eq!(x1.raw_value(), y1.raw_value());

        // A point can be constructed only given correct coordinates
        // This was a source of some nasty bugs
        let coordinates = Coordinates { x: x1, y: y1 };

        assert_eq!(coordinates.x, Coordinate::<X>::new(10));
        assert_eq!(coordinates.y, Coordinate::<Y>::new(10));

        // All of the below should not and will not work. Will produce compile
        // time errors similar to this:
        //
        // mismatched types:
        // expected struct `new_coordinates::Coordinate<new_coordinates::X>`
        //    found struct `new_coordinates::Coordinate<new_coordinates::Y>`
        //
        // ``` compile_fail
        // assert_eq!(x1, y1);
        // ```
        //
        // ``` compile_fail
        // let coordinates = Coordinates { x: y2, y: x2 };
        // ```
        //
        // ``` compile_fail
        // assert_eq!(y2.distance(&x1), 30);
        // ```
        //
        // TODO: Use trybuild to assert compile errors
    }
}

#[cfg(test)]
mod coordinates_tests {
    use super::*;

    #[test]
    fn default() {
        let a = Coordinates::default();
        assert_eq!(a, Coordinates::new(Coordinate::new(0), Coordinate::new(0)));
    }

    #[test]
    fn shifting() {
        let mut a = Coordinates::default();
        a.shift(4, &Direction::East).shift(5, &Direction::South);
        assert_eq!(a, Coordinates::new(Coordinate::new(4), Coordinate::new(5)));
    }
}
