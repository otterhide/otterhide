/// A collection of custom widgets
use bevy_egui::egui;
use bevy_egui::egui::{Color32, RichText};

pub fn ui_error(ui: &mut egui::Ui, text: String) -> egui::Response {
    ui.label(RichText::new(text).monospace().color(Color32::RED))
}

pub trait WithWidgets {
    fn error(&mut self, text: String) -> egui::Response;
}

impl WithWidgets for egui::Ui {
    fn error(&mut self, text: String) -> egui::Response {
        self.label(RichText::new(text).monospace().color(Color32::RED))
    }
}

#[macro_export]
macro_rules! ui_error {
    ($ui: expr, $($arg:tt)*) => {
        let formatted = format!($($arg)*);
        $ui.error(formatted)
    };
}
