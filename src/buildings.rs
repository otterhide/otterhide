use crate::date::SimulationFrameCounter;
use crate::districts::District;
use crate::history::Snapshot;
use crate::major_state::MajorState;
use crate::major_state::PreloadedAssets;
use crate::parcels::{Parcel, ParcelNumber};
use crate::population::ChildIsBorn;
use crate::population::MovedOut;
use crate::population::{MovedIn, Person, PersonId};
use crate::selecting::Selected;
use bevy::ecs::system::SystemId;
use bevy::gltf::Gltf;
use bevy::prelude::*;
use bevy::utils::{HashMap, HashSet, Uuid};
use bevy_mod_picking::prelude::*;
use derive_more::Display;
use derive_more::From;
use itertools::Itertools;
use rand::seq::IteratorRandom;
use rand::{thread_rng, Rng};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::ops::Not;
use std::str::FromStr;

pub struct BuildingsPlugin;

impl Plugin for BuildingsPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ConstructionOrder>()
            .init_resource::<BuildingsRegister>()
            .register_type::<HousingCapacity>()
            .register_type::<IsOvercrowded>()
            .register_type::<HasSpareCapacity>()
            .register_type::<BuildingsRegister>()
            .init_resource::<BuildingScenes>()
            .register_type::<BuildingScenes>()
            .register_type::<BuildingClass>()
            .register_type::<BuildingId>()
            .add_systems(Startup, setup_assets)
            .add_systems(Startup, register_buildings_systems)
            .add_systems(OnEnter(MajorState::GettingReady), setup_building_scenes)
            .add_systems(
                Update,
                order_construction.run_if(
                    in_state(MajorState::Simulating)
                        .and_then(resource_changed::<SimulationFrameCounter>),
                ),
            )
            .add_systems(
                Update,
                register_newborn_residents.run_if(on_event::<ChildIsBorn>()),
            )
            .add_systems(
                Update,
                move_into_houses
                    .run_if(on_event::<MovedIn>())
                    .after(receive_orders),
            )
            .add_systems(
                Update,
                move_out_of_houses
                    .run_if(on_event::<MovedOut>())
                    .after(move_into_houses),
            )
            .add_systems(Update, receive_orders);
    }
}

fn register_newborn_residents(
    mut child_birth: EventReader<ChildIsBorn>,
    systems: Res<BuildingsSystems>,
    mut commands: Commands,
) {
    for ChildIsBorn { child } in child_birth.read() {
        if let Some(residence) = child.residence {
            let moved_in = MovedIn {
                who: child.id,
                where_to: residence,
            };
            commands.run_system_with_input(systems.register_resident, moved_in);
        }
    }
}
fn move_into_houses(
    mut moved_in: EventReader<MovedIn>,
    systems: Res<BuildingsSystems>,
    mut commands: Commands,
) {
    for event in moved_in.read() {
        commands.run_system_with_input(systems.register_resident, event.clone())
    }
}

fn move_out_of_houses(
    mut moved_out: EventReader<MovedOut>,
    systems: Res<BuildingsSystems>,
    mut commands: Commands,
) {
    for event in moved_out.read() {
        commands.run_system_with_input(
            systems.remove_resident,
            (event.where_from.clone(), event.who.clone()),
        )
    }
}

#[derive(Resource)]
struct BuildingAssets(Handle<Gltf>);

#[derive(Resource, Clone, Default, Debug, Reflect)]
#[reflect(Resource)]
pub struct BuildingsRegister(pub HashMap<BuildingId, Entity>);

#[derive(Resource, Debug)]
pub struct BuildingsSystems {
    pub construct_building: SystemId<BuildingDescription>,
    pub register_resident: SystemId<MovedIn>,
    pub remove_resident: SystemId<(BuildingId, PersonId)>,
}

fn register_buildings_systems(world: &mut World) {
    let construct_building = world.register_system(construct_building);
    let register_resident = world.register_system(register_resident);
    let remove_resident = world.register_system(remove_resident);

    world.insert_resource(BuildingsSystems {
        construct_building,
        register_resident,
        remove_resident,
    });
}

fn register_resident(
    In(moved_in): In<MovedIn>,
    mut buildings: Query<(&HousingCapacity, Option<&mut Residents>)>,
    buildings_register: ResMut<BuildingsRegister>,
    mut commands: Commands,
) {
    debug!("Registering resident {moved_in:#?}");
    let Some(building) = buildings_register.0.get(&moved_in.where_to) else {
        panic!(
            "Building {id:#?} not in the registry {buildings_register:#?}",
            id = moved_in.where_to
        );
    };

    let (capacity, residents) = buildings.get_mut(*building).unwrap();
    match residents {
        None => {
            let residents = Residents(HashSet::from_iter(vec![moved_in.who.clone()]));
            commands.entity(*building).insert(residents);
            // NOTE: We assume that empty building has capacity > 1, so no need to mark it with IsOvercrowded or remove HasSpareCapacity. Wrong?
        }
        Some(mut residents) => {
            residents.0.insert(moved_in.who.clone());
            if residents.0.len() >= capacity.0 as usize {
                commands.entity(*building).remove::<HasSpareCapacity>();
            }
            if residents.0.len() > capacity.0 as usize {
                commands.entity(*building).insert(IsOvercrowded);
            }
        }
    }
}

fn remove_resident(
    In((building, person)): In<(BuildingId, PersonId)>,
    mut buildings: Query<(&HousingCapacity, Option<&mut Residents>), With<Building>>,
    buildings_register: ResMut<BuildingsRegister>,
    mut commands: Commands,
) {
    debug!("Removing resident {person:?} from building {building:?}");
    let Some(building) = buildings_register.0.get(&building) else {
        panic!("Building {building:#?} not in the registry {buildings_register:#?}");
    };

    let (capacity, residents) = buildings.get_mut(*building).unwrap();
    match residents {
        None => {
            error!("There are no residents in building {building:?}. Can't remove {person:?}.");
        }
        Some(mut residents) => {
            if residents.0.remove(&person).not() {
                error!("The person {person:?} is not a resident at {building:?}. Residents are {residents:#?}");
            }
            if residents.0.len() <= capacity.0 as usize {
                commands.entity(*building).remove::<IsOvercrowded>();
            }
            if residents.0.len() < capacity.0 as usize {
                commands.entity(*building).insert(HasSpareCapacity);
            }
            if residents.0.is_empty() {
                commands.entity(*building).remove::<Residents>();
            }
        }
    }
}

fn setup_assets(
    assets: Res<AssetServer>,
    mut commands: Commands,
    mut preloaded: ResMut<PreloadedAssets>,
) {
    const ASSET_PATH: &str = "buildings.glb";
    info!("Loading {ASSET_PATH} asset");
    let handle = assets.load(ASSET_PATH);
    preloaded.0.insert(handle.clone().untyped());
    commands.insert_resource(BuildingAssets(handle));
}

#[derive(Resource, Default, Debug, Reflect)]
#[reflect(Resource)]
pub struct BuildingScenes(pub HashMap<BuildingVariant, Handle<Scene>>);

impl BuildingScenes {
    pub fn classes(&self) -> Vec<String> {
        self.0
            .keys()
            .map(|BuildingVariant { class, .. }| class)
            .unique()
            .cloned()
            .collect()
    }

    pub fn get_class(&self, class: &str) -> HashMap<BuildingVariant, Handle<Scene>> {
        self.0
            .clone()
            .iter()
            .filter_map(|(variant, scene)| {
                if variant.class == class {
                    Some((variant.clone(), scene.clone()))
                } else {
                    None
                }
            })
            .collect()
    }
}

fn setup_building_scenes(
    building_assets: Res<BuildingAssets>,
    assets: Res<Assets<Gltf>>,
    mut scenes: ResMut<BuildingScenes>,
) {
    let buildings = assets.get(&building_assets.0).unwrap();
    for (name, scene) in &buildings.named_scenes {
        debug!("Looking for buildings in scene {name}");
        let Ok(variant) = name.parse::<BuildingVariant>() else {
            warn!("Scene name {name} cannot be parsed as a building variant");
            continue;
        };

        scenes.0.insert(variant, scene.clone());
    }

    let classes = scenes
        .classes()
        .iter()
        .map(|class| format!("  - {class}"))
        .join("\n");
    info!("Building classes registered:\n{classes}");
}

/// This event represents a decision to construct a new building.
///
/// It will be stored in the history, and will result in spawning a new building.
#[derive(Event, Clone, Debug, Serialize, Deserialize)]
pub struct ConstructionOrder(pub BuildingDescription);

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BuildingDescription {
    pub id: BuildingId,
    pub transform: Transform,
    pub variant: BuildingVariant,
    pub address: String,
    pub capacity: HousingCapacity,
    pub residents: Option<HashSet<PersonId>>,
}

/// A tag for building entities
#[derive(Component, Serialize, Deserialize)]
pub struct Building;

#[derive(Bundle)]
pub struct BuildingBundle {
    id: BuildingId,
    variant: BuildingVariant,
    scene: SceneBundle,
    name: Name,
    class: BuildingClass,
    capacity: HousingCapacity,
    marker: Building,
}

impl BuildingBundle {
    pub fn new(description: BuildingDescription, scenes: &BuildingScenes) -> Self {
        let BuildingDescription {
            id,
            address,
            variant,
            transform,
            capacity,
            residents: _,
        } = description;
        let scene = scenes.0.get(&variant).unwrap().clone();
        let name = address.into();
        let class = BuildingClass(variant.class.clone());
        Self {
            id,
            variant,
            name,
            class,
            scene: SceneBundle {
                scene,
                transform,
                ..default()
            },
            capacity,
            marker: Building,
        }
    }
}

#[derive(
    Component,
    Hash,
    PartialEq,
    Eq,
    Clone,
    Copy,
    Debug,
    From,
    Display,
    Reflect,
    Serialize,
    Deserialize,
)]
pub struct BuildingId(pub Uuid);

/// Who lives in the building?
///
/// Take care to keep in sync with persons::Residence component.
#[derive(Component, Debug, Clone, Reflect, From, Serialize, Deserialize)]
pub struct Residents(pub HashSet<PersonId>);

/// How many people can this building house in total?
#[derive(Component, Debug, Clone, Reflect, Serialize, Deserialize)]
pub struct HousingCapacity(pub u8);

/// Marker for buildings with more residents than capacity
///
/// When residents change, take care to update it if necessary.
#[derive(Component, Debug, Clone, Reflect)]
pub struct IsOvercrowded;

/// Marker for buildings with less residents than capacity
///
/// When residents change, take care to update it if necessary.
#[derive(Component, Debug, Clone, Reflect)]
pub struct HasSpareCapacity;

/// A variant identifies the building scene
///
/// Variants come from the buildings.blend file (and buildings.glb exported from
/// Blender). They are parsed from scene names.
#[derive(Component, Debug, Clone, Hash, PartialEq, Eq, Reflect, Serialize, Deserialize)]
pub struct BuildingVariant {
    pub class: String,

    /// The numerical value to distinguish between variants withing the same class
    ///
    /// The .001, .002, etc. part in Blender's scene name, for example 2 in
    /// «building-house-small.002». It doesn't have any semantic meaning, other
    /// than to distinguish between otherwise equivalent building variants.
    pub number: u16,
}

#[derive(Component, Reflect, Debug, Clone)]
#[reflect(Component)]
pub struct BuildingClass(pub String);

impl Display for BuildingVariant {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Self { class, number } = self;
        write!(f, "{class}.{number}")
    }
}

impl FromStr for BuildingVariant {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // TODO: Make invalid regexp a compile time error
        let pattern = Regex::new(r"^building-(?<class>.+)\.(?<appearance>\d+)$").unwrap();

        pattern
            .captures(s)
            .and_then(|captured| {
                let class = captured["class"].to_string();
                let number = captured["appearance"].parse().ok()?;
                Self { class, number }.into()
            })
            .ok_or("Can't parse '{s}' as a building variant.".to_string())
    }
}

/// Issue a construction order, for the record and effect
fn order_construction(
    mut build_events: EventWriter<ConstructionOrder>,
    people: Query<&Person>,
    parcels: Query<
        (
            Entity,
            &GlobalTransform,
            &Parent,
            &ParcelNumber,
            &BuildingClass,
        ),
        With<Parcel>,
    >,
    districts: Query<&Name, With<District>>,
    buildings: Query<&Building>,
    mut commands: Commands,
    scenes: Res<BuildingScenes>,
) {
    let count = parcels.iter().count();
    debug!("There are {count} parcels now.");

    let population = people.iter().count();
    let capacity = buildings.iter().count();
    let housing_shortage = population.saturating_sub(capacity) as u32;

    // Random chance of building a house: shortage / 100
    let denominator = 100;
    let numerator = housing_shortage.min(denominator);

    if thread_rng().gen_ratio(numerator, denominator) {
        let Some((entity, transform, parent, number, class)) =
            parcels.iter().choose(&mut thread_rng())
        else {
            warn!("No more parcels to build house!");
            return;
        };
        let scenes = &scenes.get_class(&class.0);
        let id = Uuid::new_v4().into();
        let variant = scenes.keys().choose(&mut rand::thread_rng()).unwrap();
        let district_name = districts.get(**parent).unwrap().to_string();
        let building_number = number.0.to_string();
        let address = format!("{building_number} {district_name}");
        commands.entity(entity).despawn();

        // Order the construction
        let description = BuildingDescription {
            id,
            address,
            transform: transform.to_owned().into(),
            variant: variant.clone(),
            capacity: HousingCapacity(2),
            residents: None,
        };
        debug!("Building a new house {description:#?}");
        build_events.send(ConstructionOrder(description));
    }
}

/// Implement the construction orders
fn receive_orders(
    mut commands: Commands,
    mut construction_orders: EventReader<ConstructionOrder>,
    systems: Res<BuildingsSystems>,
) {
    for ConstructionOrder(description) in construction_orders.read() {
        commands.run_system_with_input(systems.construct_building, description.clone());
    }
}

/// Implement the construction orders
fn construct_building(
    In(description): In<BuildingDescription>,
    mut commands: Commands,
    scenes: Res<BuildingScenes>,
    mut register: ResMut<BuildingsRegister>,
) {
    debug!("Constructing {description:#?}",);
    let id = description.id;
    let HousingCapacity(capacity) = description.capacity.clone();
    let residents = description.residents.clone().unwrap_or_default();

    let mut entity = commands.spawn((
        BuildingBundle::new(description, &scenes),
        On::<Pointer<Click>>::run(move |mut selected: ResMut<Selected>| {
            selected.ids.insert(id.into());
        }),
    ));

    if residents.len() > capacity as usize {
        entity.insert(IsOvercrowded);
    };
    if residents.len() < capacity as usize {
        entity.insert(HasSpareCapacity);
    };
    if residents.is_empty().not() {
        entity.insert(Residents(residents));
    }

    let entity = entity.id();

    debug!("Registering the new house {id:#?} -> {entity:#?}");

    register.0.insert(id, entity);
}

pub type BuildingsSnapshot = Vec<BuildingDescription>;
impl Snapshot for BuildingsSnapshot {
    fn capture(world: &mut World) -> Self {
        world
            .query_filtered::<(
                &BuildingId,
                &Name,
                &BuildingVariant,
                &Transform,
                &HousingCapacity,
                Option<&Residents>,
            ), With<Building>>()
            .iter(world)
            .map(
                |(id, name, variant, transform, capacity, residents)| BuildingDescription {
                    id: id.clone(),
                    address: name.to_string(),
                    variant: variant.clone(),
                    transform: *transform,
                    capacity: capacity.clone(),
                    residents: residents.map(|component| component.0.clone()),
                },
            )
            .collect()
    }

    fn restore(&self, world: &mut World) {
        world.resource_scope(|world, mut register: Mut<BuildingsRegister>| {
            for entity in register.0.values() {
                despawn_with_children_recursive(world, *entity)
            }
            register.0.clear();
        });

        world.resource_scope(|world, systems: Mut<BuildingsSystems>| {
            for description in self {
                world
                    .run_system_with_input(systems.construct_building, description.to_owned())
                    .unwrap();
            }
        });
    }
}
