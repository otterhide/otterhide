use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::ops::{Div, Neg, Sub};

const HOUR: f32 = 1.0 / 24.0;
const MINUTE: f32 = HOUR / 60.0;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
pub enum Month {
    January = 1,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December,
}

impl Display for Month {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

impl Month {
    fn new(number: i32) -> Self {
        match number.rem_euclid(12) {
            0 => Self::December,
            1 => Self::January,
            2 => Self::February,
            3 => Self::March,
            4 => Self::April,
            5 => Self::May,
            6 => Self::June,
            7 => Self::July,
            8 => Self::August,
            9 => Self::September,
            10 => Self::October,
            11 => Self::November,
            _ => panic!("Euclidian remainder of 12 outside of 0..11 range"),
        }
    }
}

impl From<Month> for f32 {
    fn from(val: Month) -> Self {
        (val as isize - 1) as f32
    }
}

#[cfg(test)]
mod month_tests {
    use super::*;

    #[test]
    fn month_to_number() {
        assert_eq!(Month::January as i32, 1);
        assert_eq!(Month::February as i32, 2);
        assert_eq!(Month::December as i32, 12);
    }

    #[test]
    fn number_to_month() {
        assert_eq!(Month::new(1), Month::January);
        assert_eq!(Month::new(2), Month::February);
        assert_eq!(Month::new(12), Month::December);

        // Overflow
        assert_eq!(Month::new(13), Month::January);
        assert_eq!(Month::new(14), Month::February);

        // Negative
        assert_eq!(Month::new(0), Month::December);
        assert_eq!(Month::new(-1), Month::November);
        assert_eq!(Month::new(-13), Month::November);
    }
}

/// Represents a month in a day
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Serialize, Deserialize)]
pub struct DayMonth {
    /// The current year
    year: i32,
    /// How many daymonths have passed since the start of the year.
    ///
    /// From 0.0 (inclusive) to 12.0 (exclusive).
    month: f32,
}

impl DayMonth {
    /// Initialize the day-month. Returns beginning (midnight) of the January of a given start year.
    pub fn new(year: i32) -> Self {
        Self { year, month: 0.0 }
    }

    /// Set the time of day to a value between 0.0 (last midnight) to 24.0 (next midnight)
    ///
    /// Giving a negative value or value above 24.0 should do the sensible thing,
    /// i.e. decrement or increment the month and year.
    pub fn set_hour(&mut self, hour: f32) -> &mut Self {
        self.set_time_of_day(hour / 24.0)
    }

    /// Set the time of day to a value between 0.0 (last midnight) to 1.0 (next midnight)
    ///
    /// Giving a negative value or value above 1.0 should do the sensible thing,
    /// i.e. decrement or increment the month and year.
    pub fn set_time_of_day(&mut self, time_of_day: f32) -> &mut Self {
        self.month = self.month.floor();
        self.advance(&Duration {
            months: time_of_day,
        })
    }

    pub fn advance(&mut self, duration: &Duration) -> &mut Self {
        let month = self.month + duration.months;

        self.month = month.rem_euclid(12.0);
        self.year += month.div_euclid(12.0).floor() as i32;
        self
    }

    /// Advance to the beginning of the next month
    pub fn advance_to_next(&mut self) -> &mut Self {
        self.advance(&Duration::from_months(1.0));
        self.month = self.month.floor();
        self
    }

    pub fn year(&self) -> i32 {
        self.year
    }

    pub fn month(&self) -> Month {
        Month::new(self.month as i32 + 1)
    }

    // A private helper for hour and minute
    fn minutes_i32(&self) -> i32 {
        (self.month.rem_euclid(1.0) * 60.0 * 24.0) as i32
    }

    pub fn hour(&self) -> i32 {
        self.minutes_i32().div_euclid(60)
    }

    pub fn minute(&self) -> i32 {
        self.minutes_i32().rem_euclid(60)
    }

    /// Return a number between 0.0 and 1.0 representing the fraction of the day that passed
    pub fn time_of_day(&self) -> f32 {
        self.month.rem_euclid(1.0)
    }
}

impl Display for DayMonth {
    /// Should give something like March 2024 13:32
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} {} {:02}:{:02.0}",
            self.month(),
            self.year(),
            self.hour(),
            self.minute(),
        )
    }
}

impl From<&DayMonth> for f32 {
    fn from(value: &DayMonth) -> Self {
        (value.year * 12) as f32 + value.month
    }
}

impl Sub<&DayMonth> for &DayMonth {
    type Output = Duration;

    fn sub(self, rhs: &DayMonth) -> Self::Output {
        let months: f32 = f32::from(self) - f32::from(rhs);
        Duration::from_months(months)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Serialize, Deserialize)]
pub struct Duration {
    pub months: f32,
}

impl Duration {
    pub fn new(from: &DayMonth, until: &DayMonth) -> Duration {
        let years = until.year - from.year;
        let months = until.month - from.month;
        Self {
            months: (years * 12) as f32 + months,
        }
    }

    pub fn from_years(years: f32) -> Self {
        Self {
            months: years * 12.0,
        }
    }

    pub const fn from_months(months: f32) -> Self {
        Self { months }
    }

    pub fn from_hours(hours: f32) -> Self {
        Self {
            months: hours * HOUR,
        }
    }

    pub fn from_minutes(minutes: f32) -> Self {
        Self {
            months: minutes * MINUTE,
        }
    }

    pub const fn years(&self) -> i32 {
        self.months as i32 / 12
    }
}

impl Neg for Duration {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            months: self.months.neg(),
        }
    }
}

impl Div for Duration {
    type Output = f32;

    fn div(self, rhs: Self) -> Self::Output {
        self.months / rhs.months
    }
}

#[cfg(test)]
mod duration_tests {
    use super::*;

    #[test]
    fn new() {
        let from = DayMonth {
            year: 1903,
            month: Month::April.into(),
        };
        let until = DayMonth {
            year: 1905,
            month: Month::February.into(),
        };
        let expected = Duration { months: 22.0 };
        let actual = Duration::new(&from, &until);
        assert_eq!(expected, actual);

        let from = DayMonth {
            year: 1905,
            month: Month::February.into(),
        };
        let until = DayMonth {
            year: 1903,
            month: Month::April.into(),
        };
        let expected = Duration { months: -22.0 };
        let actual = Duration::new(&from, &until);
        assert_eq!(expected, actual);

        let from = DayMonth {
            year: 1992,
            month: Month::June.into(),
        };
        let until = DayMonth {
            year: 1992,
            month: Month::June.into(),
        };
        let expected = Duration { months: 0.0 };
        let actual = Duration::new(&from, &until);
        assert_eq!(expected, actual);
    }

    #[test]
    fn compare() {
        let a = Duration::from_hours(1.0);
        let b = Duration::from_hours(2.0);

        assert!(a < b);
        assert!(b > a);

        // TODO: Figure out how to implement Ord for Duration and have .min and .max work
        // assert_eq!(a.clone().min(b.clone()), a);
        // assert_eq!(a.clone().max(b.clone()), b);
    }
}

#[cfg(test)]
mod daymonth_tests {
    use super::*;

    #[test]
    fn new() {
        let daymonth = DayMonth::new(1972);

        assert_eq!(daymonth.year(), 1972);
        assert_eq!(daymonth.month(), Month::January);
        assert_eq!(daymonth.hour(), 0);
        assert_eq!(daymonth.minute(), 0);
    }

    #[test]
    fn advance_and_format() {
        let mut daymonth = DayMonth::new(1984);
        assert_eq!(format!("{daymonth}"), "January 1984 00:00");

        daymonth.advance(&Duration::from_months(1.0));
        assert_eq!(format!("{daymonth}"), "February 1984 00:00");

        daymonth.advance(&Duration::from_minutes(45.));
        assert_eq!(format!("{daymonth}"), "February 1984 00:45");

        daymonth.advance(&Duration::from_hours(23.));
        assert_eq!(format!("{daymonth}"), "February 1984 23:45");

        daymonth.advance(&Duration::from_minutes(30.));
        assert_eq!(format!("{daymonth}"), "March 1984 00:15");

        daymonth.advance(&Duration::from_hours(13.5));
        assert_eq!(format!("{daymonth}"), "March 1984 13:45");

        daymonth.advance(&Duration::from_hours(12.));
        assert_eq!(format!("{daymonth}"), "April 1984 01:45");
    }

    #[test]
    fn order_and_compare() {
        assert_eq!(DayMonth::new(2000), DayMonth::new(2000));

        assert!(DayMonth::new(2000) < DayMonth::new(2001));
        assert!(DayMonth::new(2001) > DayMonth::new(2000));
        assert!(DayMonth::new(2000) < *DayMonth::new(2000).advance(&Duration::from_months(1.0)));
        assert!(DayMonth::new(2001) < *DayMonth::new(2000).advance(&Duration::from_months(12.5)));
        assert!(DayMonth::new(2000) > *DayMonth::new(2000).advance(&Duration::from_months(-0.1)));
    }
}
