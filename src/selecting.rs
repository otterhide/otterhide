use crate::buildings::{BuildingId, BuildingsRegister, Residents};
use crate::date::Date;
use crate::egui_widgets::WithWidgets;
use crate::population::{Adult, BirthDate, PersonId, PersonsRegister, Residence, Senior, Sex};
use bevy::ecs::system::SystemId;
use bevy::prelude::*;
use bevy::utils::HashSet;
use bevy_egui::egui::RichText;
use bevy_egui::{egui, EguiContexts};
use derive_more::From;
use std::ops::Sub;

pub struct SelectingPlugin;

impl Plugin for SelectingPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Selected>()
            .add_systems(Startup, register_selecting_systems)
            .add_systems(
                Update,
                draw_selected_windows.run_if(resource_exists::<Date>),
            );
    }
}

#[derive(Clone, Copy, Debug, From, Hash, PartialEq, Eq)]
pub enum SelectedId {
    Building(BuildingId),
    Person(PersonId),
}

/// Marker for anything that is selected
#[derive(Resource, Debug, Clone, Default)]
pub struct Selected {
    pub ids: HashSet<SelectedId>,
}

#[derive(Resource, Debug)]
pub struct SelectingSystems {
    pub select: SystemId<SelectedId>,
}

fn register_selecting_systems(world: &mut World) {
    let select = world.register_system(select);

    world.insert_resource(SelectingSystems { select });
}

fn select(In(id): In<SelectedId>, mut selected: ResMut<Selected>) {
    selected.ids.insert(id);
}

fn draw_selected_windows(
    mut contexts: EguiContexts,
    mut selected: ResMut<Selected>,
    mut commands: Commands,
    systems: Res<SelectingSystems>,
    buildings_register: Res<BuildingsRegister>,
    buildings: Query<(&Name, Option<&Residents>), With<BuildingId>>,
    persons_register: Res<PersonsRegister>,
    persons: Query<
        (
            &Name,
            &Sex,
            Option<&Residence>,
            &BirthDate,
            Option<&Adult>,
            Option<&Senior>,
        ),
        With<PersonId>,
    >,
    date: Res<Date>,
) {
    selected.ids.retain(|id| {
        let mut window_is_open = true;

        match id {
            SelectedId::Building(building_id) => {
                let Some(building_entity) = buildings_register.0.get(building_id) else {
                    return true;
                };

                let Ok((address, residents)) = buildings.get(*building_entity) else {
                    // TODO: Display the error message inside the window
                    // error!(
                    //     "Can't find entity {building_entity:?} for {building_id:?} among buildings"
                    // );
                    return true;
                };
                egui::Window::new(address.to_string())
                    .open(&mut window_is_open)
                    .show(contexts.ctx_mut(), |ui| {
                        ui.heading("Residents");
                        if let Some(residents) = residents {
                            for resident_id in residents.0.iter() {
                                let Some(resident_entity) = persons_register.0.get(resident_id)
                                else {
                                    ui_error!(ui, "Can't find resident {resident_id:?} in the persons register");
                                    continue
                                };
                                let Ok((name, sex, _residence, _birth_date, _adult, _senior)) = persons.get(*resident_entity)
                                else {
                                    ui_error!(ui, "Can't find resident {resident_id:?} among persons");
                                    continue
                                };
                                if ui.link(format!("{sex} {name}")).clicked() {
                                    commands.run_system_with_input(
                                        systems.select,
                                        resident_id.to_owned().into(),
                                    );
                                };
                            }
                        } else {
                            ui.label(RichText::new("No residents").italics());
                        };
                        ui.label(building_id.to_string());
                    });
            }
            SelectedId::Person(person_id) => {
                let Some(person_entity) = persons_register.0.get(person_id) else {
                    return true;
                };
                let Ok((name, _sex, residence, birth_date, adult, senior)) = persons.get(*person_entity) else {
                    return true;
                };

                egui::Window::new(name.to_string())
                    .open(&mut window_is_open)
                    .show(contexts.ctx_mut(), |ui| {
                        let age: u16 = date.0.sub(&birth_date.0).months as u16 / 12;
                        let sex_label = match _sex {
                            Sex::Male => "male",
                            Sex::Female => "female",
                        };
                        match (adult, senior) {
                            (None, None) => ui.label(format!("{age} year old child")),
                            (None, Some(_)) => ui.error("Old child".to_string()),
                            (Some(_), None) => ui.label(format!("{age} year old {sex_label}")),
                            (Some(_), Some(_)) => ui.label(format!("{age} year old senior {sex_label}")),
                        };
                        ui.heading("Residence");
                        if let Some(residence) = residence {
                            let Some(residence_entity) = buildings_register.0.get(&residence.0)
                            else {
                                ui_error!(ui, "Can't find {residence:?} in the buildings register");
                                return;
                            };
                            let Ok((address, _residents)) = buildings.get(*residence_entity) else {
                                ui_error!(ui, "Can't find {residence_entity:?} among the buildings");
                                return;
                            };
                            if ui.link(address.to_string()).clicked() {
                                commands.run_system_with_input(
                                    systems.select,
                                    residence.0.to_owned().into(),
                                );
                            };
                        } else {
                            ui.label(RichText::new("No known residence").italics());
                        };

                        ui.label(person_id.to_string());
                    });
            }
        }

        window_is_open
    });
}
