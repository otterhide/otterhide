use crate::configuration::Configuration;
use crate::date::{Date, NewMonth};
use crate::simulation::SimulationAssetHandle;
use crate::simulation::SimulationParameters;
use bevy::ecs::system::SystemId;
use bevy::prelude::*;
use bevy::utils::HashSet;
use std::ops::Not;

/// The primary state of the program
#[derive(States, Debug, Default, Clone, Copy, Hash, PartialEq, Eq)]
pub enum MajorState {
    /// While all the assets are being loaded
    ///
    /// Use the [PreloadedAssets][] resource to make the program wait for
    /// selected assets being fully loaded before switchting to [the Processing
    /// state](MajorState::Processing).
    #[default]
    Loading,

    /// While the assets are being processed
    ///
    /// If getting a particular asset can be done in one synchronous step, do it
    /// by adding a system in the `OnEnter(MajorStates::GettingReady)` schedule.
    /// Otherwise (like in the case of Simulation), use the [ReadyPredicates][]
    /// resource to make the program wait for processing that takes more than
    /// one frame.
    GettingReady,

    /// While the data set is being generated
    Simulating,

    /// While the user journeys across space and time
    Exploring,
}

/// The plugin responsible for switching between [major states](MajorState).
pub struct MajorStatePlugin;

impl Plugin for MajorStatePlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<MajorState>()
            .init_resource::<PreloadedAssets>()
            .init_resource::<ReadyPredicates>()
            .add_systems(
                PostUpdate,
                exit_loading_state.run_if(in_state(MajorState::Loading)),
            )
            .add_systems(
                PostUpdate,
                exit_getting_ready_state.run_if(in_state(MajorState::GettingReady)),
            )
            .add_systems(
                PostUpdate,
                exit_simulating_state.run_if(on_event::<NewMonth>()),
            );
    }
}

#[derive(Resource, Default, Debug)]
pub struct PreloadedAssets(pub HashSet<UntypedHandle>);

// TODO: The preload_assets function has too many responsibilities. Refactor.
/// This system controls when the program exits Loading state
fn exit_loading_state(
    server: Res<AssetServer>,
    mut state: ResMut<NextState<MajorState>>,
    current_state: Res<State<MajorState>>,
    assets: Res<PreloadedAssets>,
) {
    let total_count = assets.0.len();
    let pending_count = assets
        .0
        .iter()
        .map(|handle| handle.id())
        .filter(|id| server.is_loaded_with_dependencies(*id).not())
        .count();

    let current_state = current_state.get();
    let next_state = MajorState::GettingReady;

    if pending_count == 0 {
        info!(
            "All {total_count} assets loaded. Switching from {current_state:?} to {next_state:?}"
        );
        state.set(next_state);
    } else {
        debug!("Waiting for {pending_count} of {total_count} assets to preload",);
    }
}

/// A collection of predicates that indicate whether a certain subsystem is ready
///
/// If all predicates return true, the major state will exit the GetingReady state.
#[derive(Resource, Default)]
pub struct ReadyPredicates(pub Vec<SystemId<(), bool>>);

fn exit_getting_ready_state(world: &mut World) {
    let predicates =
        world.resource_scope(|_, predicates: Mut<ReadyPredicates>| predicates.0.clone());
    let current_state = world.resource_scope(|_, state: Mut<State<MajorState>>| *state.get());
    let is_simulation_loaded = world.get_resource::<SimulationAssetHandle>().is_some();
    let next_state = if is_simulation_loaded {
        MajorState::Exploring
    } else {
        MajorState::Simulating
    };

    let total_count = predicates.len();
    let pending_count = predicates
        .iter()
        .filter(|predicate| world.run_system(**predicate).unwrap().not())
        .count();

    if pending_count == 0 {
        info!("All {total_count} subsystem(s) ready. Switching from {current_state:?} to {next_state:?}");
        world.resource_scope(|_, mut state: Mut<NextState<MajorState>>| state.set(next_state));
    } else {
        debug!("Waiting for {pending_count} of {total_count} subsystem(s) to get ready",);
    }
}

pub fn exit_simulating_state(
    date: Res<Date>,
    mut state: ResMut<NextState<MajorState>>,
    current_state: Res<State<MajorState>>,
    simulation_parameters: Res<SimulationParameters>,
    runtime_parameters: Res<Configuration>,
) {
    let current_state = current_state.get();
    let next_state = MajorState::Exploring;

    if date.0 > simulation_parameters.end(&runtime_parameters) {
        info!("Simulation complete. Switching from {current_state:?} to {next_state:?}");
        state.set(next_state);
    }
}
