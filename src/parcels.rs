use crate::buildings::{BuildingClass, BuildingVariant};
use bevy::prelude::*;
use itertools::Itertools;

pub struct ParcelsPlugin;

impl Plugin for ParcelsPlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<Parcel>()
            .register_type::<ParcelNumber>();
    }
}

#[derive(Bundle, Debug)]
pub struct ParcelBundle {
    pub spatial: SpatialBundle,
    pub class: BuildingClass,

    /// In which district is this parcel?
    pub number: ParcelNumber,
    pub marker: Parcel,
}

#[derive(Component, Reflect, Debug)]
#[reflect(Component)]
pub struct ParcelNumber(pub u16);

#[derive(Component, Reflect, Debug)]
#[reflect(Component)]
pub struct Parcel;

/// Given a district scene from a GLTF asset modify it to have parcels
///
/// It works by replacing example buildings from the .glb file with Parcel
/// entities, that during a simulation can be replaced with Building entities.
pub fn setup_parcels(scene: &mut Scene) {
    let root = scene
        .world
        .query_filtered::<Entity, Without<Parent>>()
        .iter(&scene.world)
        .next()
        .unwrap();

    let mut query = scene.world.query::<(Entity, &Name, &Transform, &Parent)>();
    let parcels: Vec<(Entity, BuildingClass, Transform)> = query
        .iter(&scene.world)
        .filter_map(|(entity, name, transform, parent)| {
            // Only consider direct first generation to avoid nested entities
            // with names matching the pattern being taken for parcels
            if parent.get() != root {
                debug!("Skipping indirect descendant {name}");
                return None;
            };
            // Check if the name matches pattern
            let BuildingVariant { class, .. } = name.parse().ok()?;
            (entity, BuildingClass(class), *transform).into()
        })
        .collect_vec();

    let mut parcels_count = 0;
    for (entity, class, transform) in parcels {
        parcels_count += 1;
        debug!("Processing parcel {parcels_count} {entity:?}, class {class:?}");

        let number = ParcelNumber(parcels_count);

        let spatial = SpatialBundle {
            transform,
            visibility: Visibility::Visible,
            ..default()
        };

        scene
            .world
            .spawn(ParcelBundle {
                spatial,
                class,
                number,
                marker: Parcel,
            })
            .insert(Name::new(format!("Parcel {parcels_count:04}")));

        if let Some(entity) = scene.world.get_entity_mut(entity) {
            entity.despawn_recursive();
        } else {
            warn!("This entity does not exist!");
        };
    }
}
