use crate::configuration::Configuration;
use crate::major_state::MajorState;
use crate::simulation;
use crate::simulation::SimulationParameters;
use bevy::input::common_conditions::{input_just_pressed, input_just_released, input_pressed};
use bevy::input::mouse::MouseWheel;
use bevy::prelude::*;
use bevy_mod_raycast::prelude::*;
use bevy_rts_camera::{RtsCamera, RtsCameraPlugin, RtsCameraSystemSet};
use std::ops::{Div, Mul, Neg};

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(RtsCameraPlugin)
            .add_plugins(DeferredRaycastingPlugin::<PointerRay>::default())
            .insert_resource(RaycastPluginState::<PointerRay>::default().with_debug_cursor())
            .add_systems(
                OnEnter(MajorState::GettingReady),
                (setup_camera, disable_camera)
                    .chain()
                    .after(simulation::get_ready),
            )
            .add_systems(
                Update,
                record_drag_start_marker.run_if(input_just_pressed(MouseButton::Middle)),
            )
            .add_systems(
                Update,
                drop_drag_start_markers.run_if(input_just_released(MouseButton::Middle)),
            )
            .add_systems(
                Update,
                handle_drag
                    .run_if(input_pressed(MouseButton::Middle))
                    .before(RtsCameraSystemSet),
            )
            .add_systems(Update, handle_scroll.before(RtsCameraSystemSet))
            .add_systems(OnExit(MajorState::GettingReady), enable_camera);
    }
}

fn setup_camera(mut commands: Commands, parameters: Res<SimulationParameters>) {
    commands.spawn((
        Camera3dBundle {
            projection: Projection::Perspective(PerspectiveProjection {
                far: 5000.0,
                near: 5.0,
                ..default()
            }),
            ..default()
        },
        RtsCamera {
            height_max: 3000.,
            // TODO: Lower the camera once ground position is back a 0
            //
            // This value compensates for the logical "Ground" entity being
            // under the visual ground. See the ground.rs for more comments.
            height_min: 30.,

            bounds: bevy::math::bounding::Aabb2d {
                min: Vec2 {
                    x: -parameters.land_radius,
                    y: -parameters.land_radius,
                },
                max: Vec2 {
                    x: parameters.land_radius,
                    y: parameters.land_radius,
                },
            },
            ..default()
        },
        RaycastSource::<PointerRay>::new_cursor(),
    ));
}

fn disable_camera(mut cameras: Query<&mut Camera>) {
    let mut camera = cameras.single_mut();
    camera.is_active = false;
}

fn enable_camera(mut cameras: Query<&mut Camera>) {
    let mut camera = cameras.single_mut();
    camera.is_active = true;
}

#[derive(Reflect, Debug)]
pub struct PointerRay;

/// This resource stores the position of the camera focus and the oointer at the start of dragging
///
/// The positions are projected onto an xz plane. The resource may not exist while not dragging.
#[derive(Resource, Reflect, Debug)]
pub struct DragStartMarker {
    position: Vec2,
}

fn record_drag_start_marker(sources: Query<&RaycastSource<PointerRay>>, mut commands: Commands) {
    let Ok(source) = sources.get_single() else {
        return;
    };
    let Some(intersection) = ground_intersection(source) else {
        return;
    };

    let position = intersection.position().xz();

    commands.insert_resource(DragStartMarker { position });
}

fn drop_drag_start_markers(mut commands: Commands) {
    info!("Dropping drag start marker");
    commands.remove_resource::<DragStartMarker>();
}

fn handle_drag(
    mut sources: Query<(&RaycastSource<PointerRay>, &mut RtsCamera)>,
    mut gizmos: Gizmos,
    keys: Res<ButtonInput<KeyCode>>,
    marker: Option<Res<DragStartMarker>>,
    mut commands: Commands,
) {
    let Some(start) = marker else {
        warn!("Dragging, but there are no drag start markers!");
        return;
    };

    let Ok((source, mut camera)) = sources.get_single_mut() else {
        warn!("Dragging, but there is not a single camera!");
        return;
    };

    let Some(intersection) = ground_intersection(source) else {
        warn!("Cannot get ground intersection of the pointer ray to calculate camera drag.");
        return;
    };
    let dragged_position = intersection.position().xz();
    let focus_position = camera.focus.translation.xz();

    if keys.just_pressed(KeyCode::ShiftLeft) || keys.just_released(KeyCode::ShiftLeft) {
        commands.insert_resource(DragStartMarker {
            position: dragged_position,
        });
    }
    if keys.pressed(KeyCode::ShiftLeft) {
        gizmos.arrow(
            focus_position.extend(5.).xzy(),
            dragged_position.extend(5.).xzy(),
            Color::VIOLET,
        );
        gizmos.arrow(
            focus_position.extend(5.).xzy(),
            start.position.extend(5.).xzy(),
            Color::CRIMSON,
        );

        let a = start.position - focus_position;
        let b = dragged_position - focus_position;

        // Prevent rotation when pointer is very close to the pivot point
        // to avoid rapid twists.
        const DEAD_ZONE_RADIUS: f32 = 100.0;
        let length = b.length();
        let limit = DEAD_ZONE_RADIUS * (1.0 - camera.zoom.clamp(0.05, 0.95));
        gizmos.circle(
            focus_position.extend(5.0).xzy(),
            Direction3d::Y,
            limit,
            Color::CRIMSON,
        );
        if length < limit {
            commands.insert_resource(DragStartMarker {
                position: dragged_position,
            });
            return;
        }
        let angle = a.angle_between(b);

        // Slow down the rotation to prevent wobbling
        const DAMPING_FACTOR: f32 = 4.0;
        camera
            .target_focus
            .rotate_axis(Vec3::Y, angle / DAMPING_FACTOR);
    } else {
        gizmos.arrow(
            start.position.extend(5.).xzy(),
            dragged_position.extend(5.).xzy(),
            Color::ORANGE,
        );

        // Keep moving the camera until the dragged pointer is back at the starting position
        let drag_delta = dragged_position - start.position;

        // Slow down the motion and prevent overshooting and wobbling
        const DAMPING_FACTOR: f32 = 4.0;
        camera.target_focus.translation -= drag_delta.extend(0.0).xzy() / DAMPING_FACTOR;
    }
}

fn handle_scroll(
    mut scroll: EventReader<MouseWheel>,
    mut camera: Query<(&mut RtsCamera, &RaycastSource<PointerRay>)>,
    configuration: Res<Configuration>,
) {
    use bevy::input::mouse::MouseScrollUnit;

    let Ok((mut camera, source)) = camera.get_single_mut() else {
        return;
    };

    let vertical_multiplier = (10.0_f32).powf(configuration.vertical_scroll_sensitivity);
    let horizontal_multiplier = (10.0_f32).powf(configuration.horizontal_scroll_sensitivity);

    for event in scroll.read() {
        let (zoom_delta, angle) = match event.unit {
            MouseScrollUnit::Line => {
                debug!("Scrolling by lines {:03.3}", event.y);

                let zoom_delta = event.y / 40.0;
                let angle = event.x / -10.0;
                (zoom_delta, angle)
            }
            MouseScrollUnit::Pixel => {
                debug!("Scrolling by pixels {:03.3}", event.y);

                let zoom_delta = event.y.mul(vertical_multiplier).div(10000.0);
                let angle = event.x.mul(horizontal_multiplier).div(2500.0).neg();

                (zoom_delta, angle)
            }
        };
        camera.target_zoom = (camera.target_zoom + zoom_delta).clamp(0.0, 1.0);
        camera.target_focus.rotate_axis(Vec3::Y, angle);

        // Move focus toward the intersection
        let Some(intersection) = ground_intersection(source) else {
            warn!("Cannot get ground intersection of the pointer ray to calculate camera drag.");
            return;
        };

        let focus_distance = intersection.position() - camera.focus.translation;
        let current_zoom = camera.zoom;
        camera.target_focus.translation += focus_distance * zoom_delta * (current_zoom + 1.0);
    }
}

fn ground_intersection(source: &RaycastSource<PointerRay>) -> Option<IntersectionData> {
    source.intersect_primitive(bevy_mod_raycast::primitives::Primitive3d::Plane {
        point: Vec3::ZERO,
        normal: Vec3::Y,
    })
}
