use anyhow::Context;
use clap::Parser;
use otterhide::buildings::ConstructionOrder;
use otterhide::districts::NewDistrictEstablished;
use otterhide::history::{EventLogEntry, HistoricalEvent, History};
use otterhide::population::{ChildIsBorn, Died, Immigrated, MovedIn, MovedOut};
use sqlx::sqlite::{SqliteConnectOptions, SqliteJournalMode, SqlitePool, SqliteSynchronous};
use std::fs;
use std::path::PathBuf;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    /// Input .ron file
    simulation: PathBuf,
    /// Output .db file
    database: PathBuf,
    /// A .db file to use in case of failure
    #[arg(long)]
    failed_database: Option<PathBuf>,

}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();

    let input = std::fs::File::open(cli.simulation).context("Opening the input (.ron) file")?;

    let simulation: History =
        ron::de::from_reader(input).context("Parsing the input (.ron) file")?;

    let count = simulation.events.len();

    eprintln!("About to process {count} events");

    let db_filename = cli.database;
    if db_filename.exists() {
        // TODO: Have a --force CLI argument and refuse to overwrite without it
        eprintln!("Overwriting {}", db_filename.display());
        std::fs::remove_file(&db_filename).context("Removing the previous database")?;
    }

    let connect_options = SqliteConnectOptions::new()
        .create_if_missing(true)
        .journal_mode(SqliteJournalMode::Memory)
        .synchronous(SqliteSynchronous::Off)
        .filename(&db_filename);
    let pool = SqlitePool::connect_with(connect_options)
        .await
        .context("Connecting to the database")?;

    let connection = pool
        .acquire()
        .await
        .context("Acquiring connection to the database")?;

    let schema_sql = include_str!("../sqlite-export-schema.sql");
    sqlx::query(schema_sql)
        .execute(&pool)
        .await
        .context("Setting up the schema")?;

    let transaction = pool.begin().await?;

    let result = populate_database(simulation, pool)
        .await
        .context("Populating the database");

    transaction.commit().await.context("Committing the transaction")?;
    connection.close().await.context("Closing the database connection")?;

    result.or_else(|error| {
        eprintln!("Populating the database failed.");
        if let Some(failed_db_filename) = cli.failed_database {
            eprintln!("Saving the partially populated database at {}", failed_db_filename.display());
            fs::copy(db_filename, failed_db_filename)?;
        };
        Err(error)
    })

}

async fn populate_database(
    simulation: History,
    pool: sqlx::Pool<sqlx::Sqlite>,
) -> Result<(), anyhow::Error> {
    for entry in simulation.events.iter() {
        process_log_entry(entry, &pool)
            .await
            .map_err(|error| error.context(format!("Processing {entry:#?}")))?;
    }
    Ok(())
}

async fn process_log_entry(
    entry: &EventLogEntry,
    pool: &sqlx::Pool<sqlx::Sqlite>,
) -> Result<(), anyhow::Error> {
    let EventLogEntry { date, event } = entry;
    Ok(match event {
        HistoricalEvent::ConstructionOrder(ConstructionOrder(building)) => {
            sqlx::query(
                "Insert into building (
                   id ,
                   address ,
                   construction_year ,
                   construction_month ,
                   longitude ,
                   latitude
                 ) values ( ? , ? , ? , ? , ? , ? )",
            )
            .bind(building.id.to_string())
            .bind(building.address.to_string())
            .bind(date.year())
            .bind(date.month() as u8)
            .bind(building.transform.translation.x)
            .bind(building.transform.translation.z)
            .execute(pool)
            .await
            .context("Inserting a building")?;
        }
        HistoricalEvent::NewDistrictEstablished(NewDistrictEstablished(district)) => {
            sqlx::query("Insert into district (name) values (?)")
                .bind(district.name.to_string())
                .execute(pool)
                .await
                .context("Inserting a district")?;
        }
        HistoricalEvent::Immigrated(Immigrated(person)) => {
            sqlx::query(
                "Insert into person (
                   id,
                   name,
                   sex,
                   birth_year,
                   birth_month
                 ) values (?, ?, ?, ?, ?)",
            )
            .bind(person.id.to_string())
            .bind(person.name.to_string())
            .bind(person.sex.to_string())
            .bind(person.birth_date.0.year())
            .bind(person.birth_date.0.month() as u8)
            .execute(pool)
            .await
            .context(format!("Inserting a person who just immigrated"))?;

            sqlx::query(
                "Insert into migration (
                   person,
                   arrival_year,
                   arrival_month
                 ) values (?, ?, ?)",
            )
            .bind(person.id.to_string())
            .bind(date.year())
            .bind(date.month() as u8)
            .execute(pool)
            .await
            .context(format!("Inserting immigration data"))?;
        }
        HistoricalEvent::ChildIsBorn(ChildIsBorn { child }) => {
            sqlx::query(
                "Insert into person (
                   id,
                   name,
                   sex,
                   birth_year,
                   birth_month
                 ) values (?, ?, ?, ?, ?)",
            )
            .bind(child.id.to_string())
            .bind(child.name.to_string())
            .bind(child.sex.to_string())
            .bind(child.birth_date.0.year())
            .bind(child.birth_date.0.month() as u8)
            .execute(pool)
            .await
            .context("Inserting a new born person data")?;
        }
        HistoricalEvent::Died(Died { person }) => {
            sqlx::query(
                "Update person set
                   death_year = ?,
                   death_month = ?
                 where id = ?",
            )
            .bind(date.year())
            .bind(date.month() as u8)
            .bind(person.to_string())
            .execute(pool)
            .await
            .context("Registering death")?;
        }
        HistoricalEvent::MovedIn(MovedIn { where_to, who }) => {
            sqlx::query(
                "Insert into residency (
                   person,
                   building,
                   from_year,
                   from_month
                 ) values (?, ?, ?, ?)",
            )
            .bind(who.to_string())
            .bind(where_to.to_string())
            .bind(date.year())
            .bind(date.month() as u8)
            .execute(pool)
            .await
            .context(format!("Updating due to a person moving in"))?;
        }
        HistoricalEvent::MovedOut(MovedOut { where_from, who }) => {
            sqlx::query(
                "Update residency set
                   until_year = ?,
                   until_month = ?
                 where
                   person = ?
                   and building = ?
                   and until_month is null",
            )
            .bind(date.year())
            .bind(date.month() as u8)
            .bind(who.to_string())
            .bind(where_from.to_string())
            .execute(pool)
            .await
            .context(format!("Updating due to a person moving out"))?;
        }
    })
}
