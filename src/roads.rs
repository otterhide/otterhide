use crate::coordinates::{Coordinates, Direction};
use crate::ground::GroundSystems;
use crate::history::Snapshot;
use crate::major_state::MajorState;
use crate::major_state::PreloadedAssets;
use crate::simulation::SimulationParameters;
use bevy::ecs::system::SystemId;
use bevy::gltf::Gltf;
use bevy::math::bounding::Aabb3d;
use bevy::prelude::*;
use bevy::utils::hashbrown::HashSet;
use bevy::utils::HashMap;
use core::fmt;
use derive_more::{From, Into};
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::fmt::Display;

pub struct RoadsPlugin;

impl Plugin for RoadsPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Roads>()
            .add_systems(Update, draw_gizmos)
            .add_systems(Startup, setup_assets)
            .add_systems(Startup, register_road_systems)
            .add_systems(
                OnExit(MajorState::GettingReady),
                lay_initial_roads, // .after(simulation::get_ready)
            );
    }
}

fn draw_gizmos(mut gizmos: Gizmos, sections: Res<Roads>) {
    for coordinates in sections.sections.keys() {
        gizmos.cuboid(
            Transform::from_translation(Vec3::from(coordinates)).with_scale(Vec3::splat(10.0)),
            Color::RED,
        );
    }
}

#[derive(Resource)]
struct RoadAssets(Handle<Gltf>);

fn setup_assets(
    assets: Res<AssetServer>,
    mut commands: Commands,
    mut preloaded: ResMut<PreloadedAssets>,
) {
    const ASSET_PATH: &str = "roads.glb";
    info!("Loading {ASSET_PATH} asset");
    let handle = assets.load(ASSET_PATH);
    preloaded.0.insert(handle.clone().untyped());
    commands.insert_resource(RoadAssets(handle));
}

fn lay_initial_roads(
    mut commands: Commands,
    settings: Res<SimulationParameters>,
    constructors: Res<RoadsSystems>,
) {
    info!("Laying initial roads.");

    let center = Coordinates::default();
    let length = (settings.land_radius as u32 + 500) / 10;
    let mut plan = RoadPlan::default();

    for direction in [
        Direction::North,
        Direction::East,
        Direction::South,
        Direction::West,
    ] {
        plan.add_road(&center, &direction, length);
    }

    commands.run_system_with_input(constructors.implement_road_plan, plan);
}

// Spawn district is a one-shot system attached to a resource. It does what it says on the tin.

/// A collection of one-shot systems that create new roads
#[derive(Resource, Debug)]
pub struct RoadsSystems {
    pub construct_road_section: SystemId<(Coordinates, RoadSection)>,
    pub implement_road_plan: SystemId<RoadPlan>,
}

fn register_road_systems(world: &mut World) {
    let construct_road_section = world.register_system(construct_road_section);
    let implement_road_plan = world.register_system(implement_road_plan);

    world.insert_resource(RoadsSystems {
        construct_road_section,
        implement_road_plan,
    });
}

fn construct_road_section(
    In((coordinates, new_section)): In<(Coordinates, RoadSection)>,
    mut commands: Commands,
    road_assets: Res<RoadAssets>,
    assets: Res<Assets<Gltf>>,
    mut roads: ResMut<Roads>,
    mut sections: Query<&RoadSection>,
    ground_systems: Res<GroundSystems>,
) {
    if let Some(gltf) = assets.get(&road_assets.0) {
        match roads.sections.get(&coordinates) {
            None => {
                let entity = commands
                    .spawn(SceneBundle {
                        scene: gltf.scenes[new_section.scene_index()].clone(),
                        transform: Transform::from_translation(coordinates.borrow().into()),
                        ..default()
                    })
                    .insert(new_section)
                    .insert(Name::new("Road"))
                    .id();
                debug!("Laying a new road section {new_section:?} at {coordinates:?} ({entity:?})");

                roads.sections.insert(coordinates, entity);
            }
            Some(entity) => {
                debug!(
                        "Combining road section at {coordinates:?} with the new section {new_section:?} with {entity:?}"
                    );
                let mut old_section = *sections.get_mut(*entity).unwrap();
                let section = old_section.combine(&new_section).to_owned();

                // re-spawn new entity
                commands.entity(*entity).despawn_recursive();
                let entity = commands
                    .spawn(SceneBundle {
                        scene: gltf.scenes[section.scene_index()].clone(),
                        transform: Transform::from_translation(coordinates.borrow().into()),
                        ..default()
                    })
                    .insert(section)
                    .insert(Name::new("Road"))
                    .id();
                debug!("Combined road section {section:?} at {coordinates:?} ({entity:?})");

                roads.sections.insert(coordinates, entity);
            }
        }
    };

    let bound = Aabb3d::new(coordinates.borrow().into(), Vec3::splat(10.0));
    commands.run_system_with_input(ground_systems.remove_obstacles, bound);
}

fn implement_road_plan(In(plan): In<RoadPlan>, mut commands: Commands, systems: Res<RoadsSystems>) {
    for section in plan.sections {
        commands.run_system_with_input(systems.construct_road_section, section)
    }
}

// TODO: Consider wrapping the code below in roads::snapshots module

pub type RoadsSnapshot = RoadPlan;

impl Snapshot for RoadsSnapshot {
    fn capture(world: &mut World) -> Self {
        // let roads = world.resource::<Roads>();
        let mut plan = RoadPlan::default();

        let mut sections = world.query::<&RoadSection>();
        world.resource_scope(|world, roads: Mut<Roads>| {
            for (coordinates, entity) in roads.sections.clone() {
                let section = sections.get(world, entity).unwrap();
                plan.sections.insert((coordinates, *section));
            }
        });

        plan
    }

    fn restore(&self, world: &mut World) {
        world.resource_scope(|world, mut roads: Mut<Roads>| {
            for entity in roads.sections.values() {
                despawn_with_children_recursive(world, *entity);
            }
            roads.sections.clear();
        });

        let RoadsSystems {
            implement_road_plan,
            ..
        } = world.resource();
        world
            .run_system_with_input(*implement_road_plan, self.clone())
            .unwrap();
    }
}

/// Represents road sections with their positions, but without reference to entities in the world.
///
/// It's kind of a plan that shows roads that are supposed to be constructed or
/// a snapshot of existing roads.
#[derive(Debug, From, Into, Clone, Default, Serialize, Deserialize)]
pub struct RoadPlan {
    // It holds a HashSet instead of a HashMap, because two road sections can
    // occupy same coordinate, e.g. one N-S and the other E-W. Once constructed
    // (using implement_road_plan for example), they will be combined into an
    // intersection.
    sections: HashSet<(Coordinates, RoadSection)>,
}

impl RoadPlan {
    // Plan a straight road
    pub fn add_road(
        &mut self,
        from: &Coordinates,
        direction: &Direction,
        lenght: u32,
    ) -> &mut Self {
        for distance in 0..=lenght {
            let coordinates = from.clone().shift(distance as i32, direction).to_owned();
            let section = if distance == 0 {
                RoadSection::start(direction)
            } else if distance == lenght {
                RoadSection::end(direction)
            } else {
                RoadSection::middle(direction)
            };
            self.sections.insert((coordinates, section));
        }
        self
    }
}

/// This holds references to actual road entities
#[derive(Resource, Default)]
pub struct Roads {
    sections: HashMap<Coordinates, Entity>,
}

#[derive(Clone, Copy, Default, PartialEq, Component, Hash, Eq, Serialize, Deserialize)]
pub struct RoadSection(u8);

impl RoadSection {
    fn combine(&mut self, other: &Self) -> &mut Self {
        self.0 |= other.0;
        self
    }

    pub fn add_direction(&mut self, direction: &Direction) -> &mut Self {
        self.0 |= *direction as u8;
        self
    }

    pub fn scene_index(&self) -> usize {
        self.0 as usize
    }

    // Beginning of the road
    fn start(direction: &Direction) -> Self {
        *Self::default().add_direction(direction)
    }

    // Mid-section
    fn middle(direction: &Direction) -> Self {
        *Self::default()
            .add_direction(direction)
            .add_direction(&direction.opposite())
    }

    // End of the road
    fn end(direction: &Direction) -> Self {
        *Self::default().add_direction(&direction.opposite())
    }
}

impl From<Direction> for RoadSection {
    fn from(direction: Direction) -> Self {
        Self(direction as u8)
    }
}

impl From<u8> for RoadSection {
    fn from(value: u8) -> Self {
        Self(value & 0b1111)
    }
}

impl From<RoadSection> for u8 {
    fn from(value: RoadSection) -> Self {
        value.0
    }
}

impl From<RoadSection> for usize {
    fn from(value: RoadSection) -> Self {
        value.0 as Self
    }
}

impl fmt::Debug for RoadSection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "RoadSection({})", self)
    }
}

impl Display for RoadSection {
    /// Use Unicode Box Drawing to show where roads are
    ///
    /// See https://en.wikipedia.org/wiki/Box_Drawing
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let character = match self.0 {
            // Empty
            0b0000 => ' ',

            // One direction (dead end)
            0b0001 => '╹',
            0b0010 => '╺',
            0b0100 => '╻',
            0b1000 => '╸',

            // Straight road
            0b0101 => '┃',
            0b1010 => '━',

            // Corner
            0b0011 => '┗',
            0b0110 => '┏',
            0b1001 => '┛',
            0b1100 => '┓',

            // T junction
            0b0111 => '┣',
            0b1011 => '┻',
            0b1101 => '┫',
            0b1110 => '┳',

            // Cross roads
            0b1111 => '╋',

            // No other values are possible
            _ => panic!(
                "Incorrect bit field passed as a road direction: {value:b}",
                value = self.0
            ),
        };
        write!(f, "{character}")
    }
}

#[cfg(test)]
mod road_sections_tests {
    use super::*;

    #[test]
    fn sigils() {
        assert_eq!(RoadSection::default().to_string(), " ");
        assert_eq!(
            RoadSection::default()
                .add_direction(&Direction::East)
                .add_direction(&Direction::West)
                .to_string(),
            "━"
        );
        assert_eq!(
            RoadSection::default()
                .add_direction(&Direction::North)
                .add_direction(&Direction::South)
                .to_string(),
            "┃"
        );
        assert_eq!(
            RoadSection::default()
                .add_direction(&Direction::North)
                .add_direction(&Direction::South)
                .add_direction(&Direction::West)
                .to_string(),
            "┫"
        );
    }

    #[test]
    fn combine_directions() {
        assert_eq!(
            RoadSection::default()
                .combine(&RoadSection::from(Direction::East))
                .to_owned(),
            RoadSection::from(Direction::East)
        );
        assert_eq!(
            RoadSection::default()
                .combine(&RoadSection::from(Direction::East))
                .combine(&RoadSection::from(Direction::South))
                .to_owned(),
            RoadSection::default()
                .add_direction(&Direction::East)
                .add_direction(&Direction::South)
                .to_owned()
        );
    }
}
