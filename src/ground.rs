use crate::history::Snapshot;
use crate::major_state::{MajorState, PreloadedAssets};
use crate::simulation;
use bevy::ecs::system::{RunSystemOnce, SystemId};
use bevy::gltf::Gltf;
use bevy::math::bounding::{Aabb3d, IntersectsVolume};
use bevy::prelude::*;
use bevy_rts_camera::Ground;
use itertools::Itertools;
use serde::{Deserialize, Serialize};

pub struct GroundPlugin;

impl Plugin for GroundPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, register_ground_systems)
            .register_type::<Tree>()
            .register_type::<Ground>()
            .add_systems(Startup, setup_assets)
            .add_systems(
                OnEnter(MajorState::GettingReady),
                setup_ground.after(simulation::get_ready),
            );
    }
}

#[derive(Resource, Debug)]
pub struct GroundSystems {
    pub remove_obstacles: SystemId<Aabb3d>,
}

fn register_ground_systems(world: &mut World) {
    let remove_obstacles = world.register_system(remove_obstacles);

    world.insert_resource(GroundSystems { remove_obstacles });
}

fn remove_obstacles(
    In(aabb): In<Aabb3d>,
    mut obstacles: Query<(&mut Visibility, &GlobalTransform), With<Tree>>,
) {
    debug!("Removing obstacles in {aabb:#?}");

    let mut left = 0;
    let mut removed = 0;

    for (mut visibility, transform) in obstacles.iter_mut() {
        let obstacle_bound = Aabb3d::new(
            transform.translation(),
            Vec3 {
                x: 1.0,
                y: 10.0,
                z: 1.0,
            },
        );
        if aabb.intersects(&obstacle_bound) {
            debug!("Intersecting {obstacle_bound:#?} and {aabb:#?}. Removing the obstacle.");
            removed += 1;
            *visibility = Visibility::Hidden;
        } else {
            left += 1;
        }
    }
    debug!("Removed {removed} obstacles. Left {left}.")
}

#[derive(Resource)]
struct SnowglobeAssetHandle(Handle<Gltf>);

fn setup_assets(
    assets: Res<AssetServer>,
    mut commands: Commands,
    mut preloaded: ResMut<PreloadedAssets>,
) {
    const ASSET_PATH: &str = "snowglobe.glb";
    info!("Loading {ASSET_PATH} asset");
    let handle = assets.load(ASSET_PATH);
    preloaded.0.insert(handle.clone().untyped());
    commands.insert_resource(SnowglobeAssetHandle(handle));
}

#[derive(Component, Debug, Reflect)]
#[reflect(Component)]
struct Tree;

fn setup_ground(
    snowglobe_asset_handle: Res<SnowglobeAssetHandle>,
    gltf_assets: Res<Assets<Gltf>>,
    mut scenes_assets: ResMut<Assets<Scene>>,
    mut commands: Commands,
) {
    debug!("Processing snowglobe scene");

    let snowglobe_gltf = gltf_assets.get(&snowglobe_asset_handle.0).unwrap();
    let scene_handle = snowglobe_gltf.scenes.first().unwrap();
    let scene = scenes_assets.get_mut(scene_handle.clone()).unwrap();

    scene.world.run_system_once(mark_ground);

    let trees = scene
        .world
        .query::<(Entity, &Name)>()
        .iter(&scene.world)
        .filter_map(|(entity, name)| {
            // TODO: Find a way to give instances more meaningful names
            //       The GN Instance comes from Blender's GLTF export. Any
            //       instance created with geometry nodes will have this name. I
            //       could not find a way to change it.
            if name.as_str() == "GN Instance" {
                Some(entity)
            } else {
                None
            }
        })
        .collect_vec();

    for entity in trees {
        scene
            .world
            .entity_mut(entity)
            .insert(Tree)
            .insert(Name::new("Tree"));
    }

    commands.spawn((
        SceneBundle {
            scene: scene_handle.clone(),
            transform: Transform::from_scale(Vec3 {
                x: 100.0,
                y: 100.0,
                z: 100.0,
            }),
            ..default()
        },
        SnowGlobe,
        Name::new("Snow globe"),
    ));
}

fn mark_ground(objects: Query<(Entity, &Name)>, mut commands: Commands) {
    for (entity, name) in objects.iter() {
        let name = &name.as_str();
        if name.starts_with("Ground") {
            info!("Marking entity {entity:?} (named `{name}`) as Ground");
            commands.entity(entity).insert(Ground);
        }
    }
}

#[derive(Component, Debug)]
pub struct SnowGlobe;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GroundSnapshot;

impl Snapshot for GroundSnapshot {
    fn capture(_world: &mut World) -> Self {
        Self
    }

    fn restore(&self, world: &mut World) {
        let mut obstacles = world.query_filtered::<&mut Visibility, With<Tree>>();
        for mut visibility in obstacles.iter_mut(world) {
            *visibility = Visibility::Visible
        }
    }
}
