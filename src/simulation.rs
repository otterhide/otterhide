use crate::configuration::Configuration;
use crate::day_month::{DayMonth, Duration};
use crate::history::{EventLogEntry, Future};
use crate::major_state::{MajorState, PreloadedAssets};
use bevy::asset::AssetLoader;
use bevy::asset::AsyncReadExt;
use bevy::prelude::*;
use bevy_inspector_egui::InspectorOptions;
use serde::{Deserialize, Serialize};
use thiserror::Error;

pub struct SimulationLoadingPlugin;

impl Plugin for SimulationLoadingPlugin {
    fn build(&self, app: &mut App) {
        app.init_asset::<Simulation>()
            .init_asset_loader::<SimulationLoader>()
            .add_systems(Startup, load_simulation)
            .add_systems(OnEnter(MajorState::GettingReady), get_ready);
    }
}

pub fn get_ready(world: &mut World) {
    world.resource_scope(|world, assets: Mut<Assets<Simulation>>| {
        let handle = world.get_resource::<SimulationAssetHandle>();

        if let Some(handle) = handle {
            let simulation = assets.get(handle.0.clone()).unwrap();
            world.insert_resource(simulation.parameters.clone());
            world.insert_resource(Future {
                events: simulation.events.clone(),
            });
        } else {
            world.init_resource::<SimulationParameters>();
            world.init_resource::<Future>();
        }
    });
}

/// An asset storing a simulation that can be replayed and explored
#[derive(Asset, Debug, Deserialize, Serialize, TypePath)]
pub struct Simulation {
    // TODO: Save other simulation parameters, like duration
    pub events: Vec<EventLogEntry>,
    pub parameters: SimulationParameters,
}

/// Immutable parameters of a simulation.
///
/// Changing them in the middle of a running simulation will have unspecified
/// and probably horrible effects. If this parameters are changed, the
/// simulation has to be re-run.
#[derive(Debug, Clone, Resource, InspectorOptions, Serialize, Deserialize)]
pub struct SimulationParameters {
    /// How big is the landmass on which Otterhide is built
    pub land_radius: f32,
    /// Extra distance from the edge of landmass to the sun
    pub sun_gap: f32,
    /// Elevate the canter of sun's orbit for longer days
    pub sun_elevation: f32,
    /// Day-months per one second of real time during simulation
    pub frames_per_day_month: u8,
    /// What time the first frame of the day starts
    pub frame_offset: Duration,
}

impl Default for SimulationParameters {
    fn default() -> Self {
        Self {
            land_radius: 2000.,
            sun_gap: 200.,
            sun_elevation: 600.,
            frame_offset: Duration::from_hours(2.0),
            frames_per_day_month: 6,
        }
    }
}

impl SimulationParameters {
    /// The first moment to simulate
    ///
    /// NOTE: Do not use this in exploration! Take the date of the first event
    ///       instead.
    pub fn beginning(&self, configuration: &Configuration) -> DayMonth {
        *DayMonth::new(configuration.beginning as i32).advance(&self.frame_offset)
    }
    /// The last moment to simulate
    ///
    /// NOTE: Do not use this in exploration! Take the date of the last event
    ///       instead.
    pub fn end(&self, configuration: &Configuration) -> DayMonth {
        *DayMonth::new(configuration.beginning as i32 + configuration.duration as i32)
            .advance(&self.frame_offset)
    }
}

fn load_simulation(
    configuration: Res<Configuration>,
    assets: Res<AssetServer>,
    mut preloaded: ResMut<PreloadedAssets>,
    mut commands: Commands,
) {
    // Only load the asset if CLI / URL parameters ask for it
    if let Some(path) = &configuration.load {
        let handle = assets.load::<Simulation>(path);
        preloaded.0.insert(handle.clone().untyped());
        commands.insert_resource(SimulationAssetHandle(handle));
    }
}

#[derive(Resource)]
pub struct SimulationAssetHandle(pub Handle<Simulation>);

#[derive(Default)]
struct SimulationLoader;
impl AssetLoader for SimulationLoader {
    type Asset = Simulation;

    type Settings = ();

    type Error = SimulationLoaderError;

    fn load<'a>(
        &'a self,
        reader: &'a mut bevy::asset::io::Reader,
        _settings: &'a Self::Settings,
        _load_context: &'a mut bevy::asset::LoadContext,
    ) -> bevy::utils::BoxedFuture<'a, Result<Self::Asset, Self::Error>> {
        Box::pin(async move {
            // TODO: Serialize and deserialize to Simulation directly
            let mut content = Vec::new();
            reader.read_to_end(&mut content).await?;
            let simulation: Simulation = ron::de::from_bytes(&content)?;
            Ok(simulation)
        })
    }

    fn extensions(&self) -> &[&str] {
        &["ron"] // TODO: Consider using a different extension.
    }
}
#[derive(Debug, Error)]
enum SimulationLoaderError {
    #[error("Could not load asset: {0}")]
    Io(#[from] std::io::Error),

    #[error("Could not parse the simulation: {0}")]
    DecodingError(#[from] ron::error::SpannedError),
}
