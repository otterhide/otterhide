use crate::history::History;
use crate::simulation::{Simulation, SimulationParameters};
use bevy::log::info;
use ron::ser::to_string_pretty;

pub(crate) fn export(history: History, parameters: SimulationParameters) {
    let simulation = Simulation {
        events: history.events,
        parameters,
    };
    let exported = to_string_pretty(&simulation, ron::ser::PrettyConfig::default()).unwrap();
    let file_name = "simulation.ron";
    info!("Saving the simulation to {file_name}");
    save_file(file_name, exported.as_str());
}

/// In a web browser, download the file
#[cfg(target_arch = "wasm32")]
pub fn save_file(file_name: &str, content: &str) {
    use gloo_file::{Blob, ObjectUrl};
    use gloo_utils::document;
    use wasm_bindgen::prelude::*;
    use web_sys::HtmlAnchorElement;

    let blob = Blob::new(content);
    let url = ObjectUrl::from(blob);
    let href = url.to_string();

    let document = document();
    let anchor = document
        .create_element("a")
        .unwrap()
        .dyn_into::<HtmlAnchorElement>()
        .unwrap();
    anchor.set_attribute("href", &href).unwrap();
    anchor.set_attribute("download", file_name).unwrap();
    anchor.click();
}

/// On a desktop device, save to a file
#[cfg(not(target_arch = "wasm32"))]
pub fn save_file(file_name: &str, content: &str) {
    use std::fs;

    fs::write(file_name, content).unwrap();
}
