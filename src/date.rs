use crate::configuration::Configuration;
use crate::day_month::{DayMonth, Duration};
use crate::history::{EventLogEntry, Future, History, Snapshot};
use crate::major_state::MajorState;
use crate::simulation::{self, SimulationParameters};
use anyhow::Context;
use bevy::ecs::system::SystemId;
use bevy::prelude::*;
use bevy_inspector_egui::inspector_options::std_options::NumberDisplay;
use bevy_inspector_egui::prelude::*;
use serde::{Deserialize, Serialize};

pub struct DatePlugin;

impl Plugin for DatePlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<Timescale>()
            .init_resource::<Timescale>()
            .register_type::<SimulationFrameCounter>()
            .init_resource::<SimulationFrameCounter>()
            .add_event::<NewMonth>()
            .add_systems(
                OnEnter(MajorState::GettingReady),
                (initialize_date, setup_date_display)
                    .chain()
                    .after(simulation::get_ready),
            )
            .add_systems(Startup, register_date_systems)
            .add_systems(
                PreUpdate,
                advance_getting_ready_date.run_if(in_state(MajorState::GettingReady)),
            )
            .add_systems(
                PreUpdate,
                advance_exploration_date.run_if(in_state(MajorState::Exploring)),
            )
            .add_systems(
                PreUpdate,
                update_date_display.run_if(resource_exists::<Date>),
            );
    }
}

fn initialize_date(world: &mut World) {
    let future = world.resource::<Future>();
    if let Some(EventLogEntry { date, .. }) = future.events.first() {
        info!("Initializing date to {date} based of first unprocessed historical event");
        world.insert_resource(Date(*date))
    } else {
        let simulation = world.resource::<SimulationParameters>();
        let runtime = world.resource::<Configuration>();
        let date = simulation.beginning(runtime);
        info!("Initializing date to {date}");
        world.insert_resource(Date(date));
    }
}

// fn initialize_getting_ready_date(world: &mut World) {
// }

#[derive(Resource, Debug, Clone, Serialize, Deserialize)]
pub struct Date(pub DayMonth);

/// Count the frames simulated so far
#[derive(Resource, Default, Debug, Reflect)]
#[reflect(Resource)]
pub struct SimulationFrameCounter(pub u8);

#[derive(Resource, Reflect, InspectorOptions)]
#[reflect(Resource, InspectorOptions)]
// TODO: It's hard to control small fractions. Make the scale logarithmic?
struct Timescale {
    /// Day-months per one second of real time during exploration
    #[inspector(min = 0.0, max = 2.0, display = NumberDisplay::Slider)]
    scale: f32,
}

impl Default for Timescale {
    fn default() -> Self {
        Self { scale: 1.0 / 120.0 }
    }
}

#[derive(Event)]
pub struct NewMonth;

fn advance_simulation_date(
    mut date: ResMut<Date>,
    mut new_month: EventWriter<NewMonth>,
    mut framecounter: ResMut<SimulationFrameCounter>,
    simulation: Res<SimulationParameters>,
) {
    framecounter.0 = u8::rem_euclid(framecounter.0 + 1, simulation.frames_per_day_month);
    if framecounter.0 == 0 {
        date.0.advance_to_next().advance(&simulation.frame_offset);
        debug!("New simulation frame + new month: {:?}", date.0);
        // IDEA: Maybe we can get rid of it and let all systems look into the frame counter?
        new_month.send(NewMonth);
    } else {
        let duration = Duration::from_months(1.0 / simulation.frames_per_day_month as f32);
        date.0.advance(&duration);
        debug!("New simulation frame {:?}", date.0);
    }
}

fn advance_exploration_date(
    time: Res<Time>,
    mut date: ResMut<Date>,
    timescale: Res<Timescale>,
    simulation: Res<SimulationParameters>,
    history: Res<History>,
    future: Res<Future>,
) {
    let slow_down_factor: f32 = if future.events.is_empty() {
        // Do not go much over the duration, but gently slow down after
        let end = history
            .events
            .last()
            .context("Getting the last event from history to put a break on time advancement")
            .unwrap()
            .date;

        // How much time passed since the end of the simulation
        let overtime_duration = Duration::new(&end, &date.0);

        // How long does it take to slow down time to a complete halt after the
        // simulation is over
        let slowdown_duration = Duration::from_hours(7.55);

        (overtime_duration / slowdown_duration).min(1.0)
    } else {
        0.0
    };

    let scale = timescale.scale * (1.0 - slow_down_factor);

    // TODO: Figure out how to implement Ord for Duration and have .min and .max work on it
    let delta = time.delta_seconds();
    let raw_duration = delta * scale;
    let raw_duration_limit = 1.0 / simulation.frames_per_day_month as f32;
    let duration = Duration::from_months(raw_duration.min(raw_duration_limit));

    date.0.advance(&duration);
}

/// Advances the date to the first event in the future
fn advance_getting_ready_date(
    mut date: ResMut<Date>,
    future: Res<Future>,
    mut new_month: EventWriter<NewMonth>,
) {
    if let Some(entry) = future.events.first() {
        if entry.date.month() != date.0.month() {
            new_month.send(NewMonth);
        }
        date.0 = entry.date;
    }
}

// TODO: Consider if date display logic should go to a UI module?
#[derive(Component)]
struct DateDisplay;

fn setup_date_display(mut commands: Commands, date: Res<Date>) {
    commands
        .spawn(
            TextBundle::from_section(
                date.0.to_string(),
                TextStyle {
                    font_size: 32.0,
                    color: Color::WHITE,
                    ..default()
                },
            )
            .with_style(Style {
                justify_self: JustifySelf::Center,
                margin: UiRect::top(Val::Px(20.0)),
                ..default()
            }),
        )
        .insert(DateDisplay);
}

fn update_date_display(mut display: Query<&mut Text, With<DateDisplay>>, date: Res<Date>) {
    let mut display = display.single_mut();
    display.sections[0].value = date.0.to_string();
}

pub type DateSnapshot = Date;

impl Snapshot for Date {
    fn capture(world: &mut World) -> Self {
        world.resource_scope(|_, date: Mut<Date>| date.clone())
    }
    fn restore(&self, world: &mut World) {
        world.resource_scope(|_, mut date: Mut<Date>| *date = self.clone());
    }
}

// Systems exposed to other systems

#[derive(Resource, Debug)]
pub struct DateSystems {
    pub advance_simulation_date: SystemId,
}

fn register_date_systems(world: &mut World) {
    let advance_simulation_date = world.register_system(advance_simulation_date);

    world.insert_resource(DateSystems {
        advance_simulation_date,
    });
}

/// A run condition that fires once a day on a given hour, or on the next opportunity
///
/// The hour is given as a floating point number between 0 (midnight) and 24:00
/// (following midnight, exclusive). So 6.25 translates to 06:15.
pub fn past_hour(hour: f32) -> impl FnMut(Option<Res<Date>>, Local<Option<DayMonth>>) -> bool {
    // No funny business with overflowing values!
    let hour = hour.rem_euclid(24.0);

    move |date: Option<Res<Date>>, mut last_ran: Local<Option<DayMonth>>| {
        let Some(date) = date else {
            return false;
        };

        // On first run, pretend that it happened on previous daymonth
        let previous = last_ran.unwrap_or(
            *date
                .0
                .clone()
                .set_hour(hour)
                .advance(&Duration::from_months(-1.0)),
        );
        let next = *previous.clone().advance(&Duration::from_months(1.0));
        let now = date.0;

        if now >= next {
            // Do it, and pretend it happened on time
            *last_ran = Some(*now.clone().set_hour(hour));
            true
        } else {
            false
        }
    }
}
