use bevy::prelude::*;
use bevy_mod_raycast::deferred::RaycastSource;
use bevy_rts_camera::RtsCamera;

use crate::{camera::PointerRay, major_state::MajorState};

pub struct SearchlightPlugin;

impl Plugin for SearchlightPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(MajorState::GettingReady), setup_searchlight)
            .add_systems(Update, move_searchlight);
    }
}

#[derive(Component, Debug, Reflect)]
#[reflect(Component)]
pub struct Searchlight;

fn setup_searchlight(mut commands: Commands) {
    commands.spawn((
        Searchlight,
        PointLightBundle {
            point_light: PointLight {
                color: Color::hsl(120.0, 1.0, 0.8),
                intensity: 3e7,
                range: 300.0,
                shadows_enabled: true,
                ..default()
            },
            transform: Transform::from_translation(Vec3 {
                x: 0.0,
                y: 30.0,
                z: 0.0,
            }),
            ..default()
        },
        Name::new("Searchlight"),
    ));
}

fn move_searchlight(
    mut searchlight: Query<&mut Transform, With<Searchlight>>,
    ray_sources: Query<&RaycastSource<PointerRay>, With<RtsCamera>>,
    mut gizmos: Gizmos,
    time: Res<Time<Real>>,
) {
    let Ok(mut transform) = searchlight.get_single_mut() else {
        return;
    };
    gizmos.sphere(transform.translation, Quat::IDENTITY, 10.0, Color::YELLOW);

    let Ok(ray_source) = ray_sources.get_single() else {
        return;
    };

    // TODO: Use ground, trees, road and other meshes
    let Some(intersection) =
        ray_source.intersect_primitive(bevy_mod_raycast::primitives::Primitive3d::Plane {
            point: Vec3::ZERO,
            normal: Vec3::Y,
        })
    else {
        return;
    };

    let target = intersection.position() + Vec3::Y * 40.0;

    let delta = target - transform.translation;
    transform.translation += delta * (time.delta_seconds() * 4.0).max(1.0);
}
