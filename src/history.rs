use crate::buildings::{BuildingDescription, BuildingsSnapshot, ConstructionOrder};
use crate::date::{Date, DateSnapshot, NewMonth};
use crate::day_month::{DayMonth, Month};
use crate::districts::DistrictsSnapshot;
use crate::districts::NewDistrictEstablished;
use crate::ground::GroundSnapshot;
use crate::major_state::{MajorState, ReadyPredicates};
use crate::population::{ChildIsBorn, Died, Immigrated, MovedIn, MovedOut, PopulationSnapshot};
use crate::roads::RoadsSnapshot;
use bevy::ecs::system::{RunSystemOnce, SystemId};
use bevy::prelude::*;
use serde::{Deserialize, Serialize};
use std::fmt::Display;

pub struct HistoryPlugin;

impl Plugin for HistoryPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<History>()
            .init_resource::<Future>()
            .init_resource::<Snapshots>()
            .add_systems(Startup, setup_history_systems)
            .add_systems(Startup, register_ready_predicates)
            .add_systems(
                PreUpdate,
                take_snapshot.run_if(
                    in_state(MajorState::GettingReady)
                        .or_else(in_state(MajorState::Simulating))
                        .and_then(on_event::<NewMonth>()),
                ),
            )
            .add_systems(
                PostUpdate,
                register_historical_events.run_if(
                    in_state(MajorState::Simulating).or_else(in_state(MajorState::GettingReady)),
                ),
            )
            .add_systems(
                PreUpdate,
                replay_historical_events.run_if(
                    in_state(MajorState::GettingReady).or_else(in_state(MajorState::Exploring)),
                ),
            );
    }
}

fn register_ready_predicates(world: &mut World) {
    let predicate = world.register_system(history_ready);
    world.resource_scope(|_, mut predicates: Mut<ReadyPredicates>| predicates.0.push(predicate));
}

fn history_ready(future: Res<Future>) -> bool {
    future.events.is_empty()
}

#[derive(Resource, Default, Clone, Debug, Serialize, Deserialize)]
pub struct History {
    pub events: Vec<EventLogEntry>,
}

#[derive(Resource, Default, Clone, Debug, Serialize, Deserialize)]
pub struct Snapshots {
    pub collection: Vec<MainSnapshot>,
}

/// A collection of events that will happen in the future from the time traveler's perspective
///
/// Reset by time_travel. Used by reply_historical_events.
#[derive(Resource, Default, Clone)]
pub struct Future {
    pub events: Vec<EventLogEntry>,
}

pub trait Snapshot {
    /// Given a reference to the world, produce and return a snapshot
    ///
    /// NOTE: A mutable reference to the world is required for technical
    ///       reasons, but please don't actually mutate the world while taking a
    ///       snapshot. Taking a snapshot should have no side effects.
    fn capture(world: &mut World) -> Self;

    /// Given a reference to the world, restore it's state to this captured in the snapshot
    fn restore(&self, world: &mut World);
}

/// A wrapper for any kind of event that should be replayed in explore mode
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum HistoricalEvent {
    ConstructionOrder(ConstructionOrder),
    NewDistrictEstablished(NewDistrictEstablished),
    Immigrated(Immigrated),
    ChildIsBorn(ChildIsBorn),
    Died(Died),
    MovedIn(MovedIn),
    MovedOut(MovedOut),
}

/// A historical event together with it's date
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EventLogEntry {
    pub date: DayMonth,
    pub event: HistoricalEvent,
}

impl Display for HistoricalEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            HistoricalEvent::ConstructionOrder(ConstructionOrder(BuildingDescription {
                id,
                address,
                transform,
                variant,
                capacity,
                residents,
            })) => {
                let Vec3 { x, z, .. } = transform.translation;
                write!(
                    f,
                    "Construction of a new building ({variant}, {id}) with a {capacity:?} and {residents:?} residents ordered at {address} ({x:.2}, {z:.2})" ,
                )
            }
            HistoricalEvent::NewDistrictEstablished(NewDistrictEstablished(district)) => {
                write!(f, "New district established: {district:?}",)
            }
            HistoricalEvent::Immigrated(Immigrated(person)) => {
                write!(f, "A new immigrant arrived: {person:?}")
            }
            HistoricalEvent::MovedIn(MovedIn { who, where_to }) => {
                write!(f, "The person {who} moved into the building {where_to}")
            }
            HistoricalEvent::MovedOut(MovedOut { who, where_from }) => {
                write!(f, "The person {who} moved out of the building {where_from}")
            }
            HistoricalEvent::ChildIsBorn(ChildIsBorn { child }) => {
                write!(f, "The child is born {child:#?}")
            }
            HistoricalEvent::Died(Died { person: id }) => {
                write!(f, "The person {id} died")
            }
        }
    }
}

fn register_historical_events(
    mut history: ResMut<History>,
    date: Res<Date>,
    mut construction_orders: EventReader<ConstructionOrder>,
    mut new_districts: EventReader<NewDistrictEstablished>,
    mut immigrated: EventReader<Immigrated>,
    mut born: EventReader<ChildIsBorn>,
    mut moved_in: EventReader<MovedIn>,
    mut moved_out: EventReader<MovedOut>,
    mut died: EventReader<Died>,
) {
    let date = date.0;
    for event in new_districts.read() {
        let event = HistoricalEvent::NewDistrictEstablished(event.to_owned());
        debug!("Registering a historical event on {date:?}: {event:#?}");
        history.events.push(EventLogEntry { event, date });
    }
    for event in construction_orders.read() {
        let event = HistoricalEvent::ConstructionOrder(event.to_owned());
        debug!("Registering a historical event on {date:?}: {event:#?}");
        history.events.push(EventLogEntry { event, date });
    }
    for event in immigrated.read() {
        let event = HistoricalEvent::Immigrated(event.to_owned());
        debug!("Registering a historical event on {date:?}: {event:#?}");
        history.events.push(EventLogEntry { event, date });
    }
    for event in born.read() {
        let event = HistoricalEvent::ChildIsBorn(event.to_owned());
        debug!("Registering a historical event on {date:?}: {event:#?}");
        history.events.push(EventLogEntry { event, date });
    }
    for event in moved_in.read() {
        let event = HistoricalEvent::MovedIn(event.to_owned());
        debug!("Registering a historical event on {date:?}: {event:#?}");
        history.events.push(EventLogEntry { event, date });
    }
    for event in moved_out.read() {
        let event = HistoricalEvent::MovedOut(event.to_owned());
        debug!("Registering a historical event on {date:?}: {event:#?}");
        history.events.push(EventLogEntry { event, date });
    }
    for event in died.read() {
        let event = HistoricalEvent::Died(event.to_owned());
        debug!("Registering a historical event on {date:?}: {event:#?}");
        history.events.push(EventLogEntry { event, date });
    }
}

/// The main snapshot that binds them all
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MainSnapshot {
    date: Date,

    // TODO: Replace all below with a single Vec<Box<dyn Snapshot>>,
    ground: GroundSnapshot,
    buildings: BuildingsSnapshot,
    districts: DistrictsSnapshot,
    roads: RoadsSnapshot,
    population: PopulationSnapshot,
}

// TODO: Find a way to coordinate with lay_initial_roads without exposing this system
pub fn take_snapshot(world: &mut World) {
    // TODO: DRY on take_snapshot. Maybe a macro?
    let date = DateSnapshot::capture(world);
    let ground = GroundSnapshot::capture(world);
    let roads = RoadsSnapshot::capture(world);
    let districts = DistrictsSnapshot::capture(world);
    let buildings = BuildingsSnapshot::capture(world);
    let population = PopulationSnapshot::capture(world);

    debug!("Registering a new snapshot on {date:#?}");

    let snapshot = MainSnapshot {
        date,
        ground,
        buildings,
        districts,
        roads,
        population,
    };

    world.resource_scope(|_, mut snapshots: Mut<Snapshots>| {
        snapshots.collection.push(snapshot);
    })
}

fn rollback(In(snapshot): In<MainSnapshot>, world: &mut World) {
    debug!("Restoring snapshot {}", snapshot.date.0);
    // Make sure no pending events from the future are going to reach systems
    // after the rollback
    world.run_system_once(clear_pending_historical_events);

    world.resource_scope(|_, mut game_state: Mut<NextState<MajorState>>| {
        game_state.set(MajorState::Exploring);
    });

    // TODO: DRY on rollback. Maybe a macro?
    snapshot.date.restore(world);
    snapshot.ground.restore(world);
    snapshot.roads.restore(world);
    snapshot.districts.restore(world);
    snapshot.buildings.restore(world);
    snapshot.population.restore(world);

    let events = world.resource_scope(|_, history: Mut<History>| history.events.clone());

    world.resource_scope(|_, mut future: Mut<Future>| {
        future.events = events
            .clone()
            .into_iter()
            .filter(|event| event.date >= snapshot.date.0)
            .collect();

        info!(
            "Copied {} of {} events from the history to the future",
            future.events.len(),
            events.len()
        )
    });
}

fn clear_pending_historical_events(
    mut construction_orders: EventReader<ConstructionOrder>,
    mut new_districts: EventReader<NewDistrictEstablished>,
    mut immigrated: EventReader<Immigrated>,
    mut moved_in: EventReader<MovedIn>,
) {
    construction_orders.clear();
    new_districts.clear();
    immigrated.clear();
    moved_in.clear();
}

#[derive(Debug, Resource)]
pub struct HistorySystems {
    pub rollback: SystemId<MainSnapshot>,
}

fn setup_history_systems(world: &mut World) {
    let rollback = world.register_system(rollback);
    world.insert_resource(HistorySystems { rollback })
}

fn replay_historical_events(
    date: Res<Date>,
    mut future: ResMut<Future>,
    mut orders: EventWriter<ConstructionOrder>,
    mut districts: EventWriter<NewDistrictEstablished>,
    mut immigrated: EventWriter<Immigrated>,
    mut moved_in: EventWriter<MovedIn>,
    mut moved_out: EventWriter<MovedOut>,
    mut child_birth: EventWriter<ChildIsBorn>,
    mut died: EventWriter<Died>,
) {
    future.events.retain(|entry| {
        if entry.date <= date.0 {
            debug!("Replaying a historical event {entry:#?} (now is {date:#?})");

            match entry.event.clone() {
                HistoricalEvent::ConstructionOrder(order) => {
                    orders.send(order);
                }
                HistoricalEvent::NewDistrictEstablished(district) => {
                    districts.send(district);
                }
                HistoricalEvent::Immigrated(event) => {
                    immigrated.send(event);
                }
                HistoricalEvent::MovedIn(event) => {
                    moved_in.send(event);
                }
                HistoricalEvent::MovedOut(event) => {
                    moved_out.send(event);
                }
                HistoricalEvent::ChildIsBorn(event) => {
                    child_birth.send(event);
                }
                HistoricalEvent::Died(event) => {
                    died.send(event);
                }
            };
            false
        } else {
            true
        }
    });
}

// TODO: Ditch the Timestamp type.
// If we need keys at all, we can use String from a DayMonth. And maybe keys are not needed at all?
/// Simplified DayMonth that can be used as a key in a hash map
#[derive(PartialEq, PartialOrd, Ord, Eq, Hash, Clone, Copy, Debug)]
pub struct Timestamp {
    pub year: i32,
    pub month: Month,
    pub hour: i32,
    pub minute: i32,
}

impl From<DayMonth> for Timestamp {
    fn from(day_month: DayMonth) -> Self {
        Self {
            year: day_month.year(),
            month: day_month.month(),
            hour: day_month.hour(),
            minute: day_month.minute(),
        }
    }
}

impl Display for Timestamp {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{yyyy} {mmmm} {hh:02}:{mm:02}",
            yyyy = self.year,
            mmmm = self.month,
            hh = self.hour,
            mm = self.minute
        )
    }
}
