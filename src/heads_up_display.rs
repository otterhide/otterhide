use crate::buildings::Building;
use crate::buildings::BuildingClass;
use crate::buildings::BuildingId;
use crate::buildings::BuildingsRegister;
use crate::buildings::HousingCapacity;
use crate::buildings::Residents;
use crate::configuration::Configuration;
use crate::date::{Date, DateSystems};
use crate::day_month::Duration;
use crate::egui_widgets::WithWidgets;
use crate::history::Future;
use crate::history::History;
use crate::history::HistorySystems;
use crate::history::Snapshots;
use crate::major_state::MajorState;
use crate::population::BirthDate;
use crate::population::Person;
use crate::population::PersonId;
use crate::population::PersonsRegister;
use crate::population::Residence;
use crate::population::Savings;
use crate::population::Sex;
use crate::ron_export;
use crate::selecting::SelectingSystems;
use crate::simulation::SimulationParameters;
use bevy::input::common_conditions::input_just_released;
use bevy::prelude::*;
use bevy_egui::egui;
use bevy_egui::egui::epaint::Shadow;
use bevy_egui::egui::Align;
use bevy_egui::egui::Color32;
use bevy_egui::egui::Frame;
use bevy_egui::egui::Layout;
use bevy_egui::egui::Margin;
use bevy_egui::egui::ProgressBar;
use bevy_egui::egui::RichText;
use bevy_egui::egui::Slider;
use bevy_egui::egui::Stroke;
use bevy_egui::EguiContexts;
use clap::ValueEnum;
use derive_more::Display;
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;

pub struct HudPlugin;

impl Plugin for HudPlugin {
    fn build(&self, app: &mut App) {
        let getting_ready_system_set = (paint_getting_ready_progress_bar,).chain();
        let simulation_system_set = (
            auto_advance.run_if(resource_equals(AutoAdvanceSimulation(true))),
            paint_simulation_progress_bar,
        )
            .chain();
        let exploration_system_set = (
            update_slider_value,
            paint_replay_controls,
            apply_slider_value.run_if(resource_changed::<DateSliderValue>),
        )
            .chain();
        let central_panel_system_set = (
            paint_population_panel.run_if(inspector_panel_shown(&InspectorPanel::Population)),
            paint_buildings_panel.run_if(inspector_panel_shown(&InspectorPanel::Buildings)),
            // TODO: Implement other panels
            // TODO: A mechanism to hide central panel
        );

        app.register_type::<AutoAdvanceSimulation>()
            .init_resource::<AutoAdvanceSimulation>()
            .register_type::<DateSliderValue>()
            .init_resource::<DateSliderValue>()
            .register_type::<InspectorPanel>()
            .add_systems(Startup, initialize_inspector_panel)
            .add_systems(
                Update,
                cycle_inspector_panels.run_if(input_just_released(KeyCode::KeyI)),
            )
            .add_systems(
                PostUpdate,
                (
                    getting_ready_system_set.run_if(in_state(MajorState::GettingReady)),
                    simulation_system_set.run_if(in_state(MajorState::Simulating)),
                    exploration_system_set.run_if(in_state(MajorState::Exploring)),
                    central_panel_system_set.run_if(resource_exists::<Date>),
                )
                    .chain(),
            );
    }
}

fn initialize_inspector_panel(configuration: Res<Configuration>, mut commands: Commands) {
    if let Some(panel) = configuration.inspect {
        commands.insert_resource(panel)
    };
}

fn cycle_inspector_panels(panel: Option<Res<InspectorPanel>>, mut commands: Commands) {
    match panel.as_deref() {
        None => commands.insert_resource(InspectorPanel::Buildings),
        Some(InspectorPanel::Buildings) => commands.insert_resource(InspectorPanel::Population),
        Some(InspectorPanel::Population) => commands.remove_resource::<InspectorPanel>(),
    };
}

#[derive(
    Resource, Debug, Eq, PartialEq, Clone, Copy, Serialize, Deserialize, Display, ValueEnum, Reflect,
)]
pub enum InspectorPanel {
    Population,
    Buildings,
}

fn inspector_panel_shown(
    required_panel: &InspectorPanel,
) -> impl FnMut(Option<Res<InspectorPanel>>) -> bool + '_ {
    move |selected_panel: Option<Res<InspectorPanel>>| {
        selected_panel.as_deref() == Some(required_panel)
    }
}

#[derive(Resource, Debug, Default, Reflect, PartialEq, Eq)]
#[reflect(Resource)]
pub struct AutoAdvanceSimulation(bool);

#[derive(Resource, Reflect, Debug, Default)]
#[reflect(Resource)]
struct DateSliderValue(f64);

fn auto_advance(mut commands: Commands, systems: Res<DateSystems>) {
    commands.run_system(systems.advance_simulation_date);
}

fn paint_getting_ready_progress_bar(
    mut contexts: EguiContexts,
    future: Res<Future>,
    history: Res<History>,
) {
    egui::TopBottomPanel::bottom("getting_ready_controls")
        .show_separator_line(false)
        .frame(Frame {
            inner_margin: Margin {
                bottom: 20.,
                top: 0.,
                left: 40.,
                right: 40.,
            },
            shadow: Shadow::NONE,
            stroke: Stroke::NONE,
            ..default()
        })
        .show(contexts.ctx_mut(), |ui| {
            let size = ui.available_size();
            egui::Grid::new("getting_ready_controls_grid")
                .min_col_width(size.x)
                .show(ui, |ui| {
                    ui.horizontal(|ui| ui.label("Processing historical events..."));

                    ui.end_row();

                    ui.horizontal(|ui| {
                        let history_events_count = history.events.len();
                        let future_events_count = future.events.len();
                        let total_events_count = history_events_count + future_events_count;
                        let progress = history_events_count as f32 / total_events_count as f32;
                        ui.add(
                            ProgressBar::new(progress)
                                .text(format!("{history_events_count} / {total_events_count}"))
                                .desired_width(size.x)
                                .desired_height(12.0),
                        );
                    });
                });
        });
}

fn paint_simulation_progress_bar(
    mut contexts: EguiContexts,
    date: Res<Date>,
    mut auto_advance: ResMut<AutoAdvanceSimulation>,
    mut commands: Commands,
    simulation_parameters: Res<SimulationParameters>,
    runtime_parameters: Res<Configuration>,
    systems: Res<DateSystems>,
) {
    egui::TopBottomPanel::bottom("simulation_controls")
        .show_separator_line(false)
        .frame(Frame {
            inner_margin: Margin {
                bottom: 20.,
                top: 0.,
                left: 40.,
                right: 40.,
            },
            shadow: Shadow::NONE,
            stroke: Stroke::NONE,
            ..default()
        })
        .show(contexts.ctx_mut(), |ui| {
            let size = ui.available_size();
            egui::Grid::new("simulation_controls_grid")
                .min_col_width(size.x)
                .show(ui, |ui| {
                    ui.horizontal(|ui| {
                        let min_button_size = egui::Vec2::new(32., 26.);
                        if auto_advance.0 {
                            let pause_button =
                                ui.add(egui::Button::new("⏸").min_size(min_button_size));
                            if pause_button.clicked() {
                                auto_advance.0 = false;
                            }
                        } else if ui.input(|i| i.modifiers.ctrl) {
                            // FIXME: Missing glyph
                            let step_button =
                                ui.add(egui::Button::new("⏯").min_size(min_button_size));
                            if step_button.clicked() {
                                commands.run_system(systems.advance_simulation_date);
                            }
                        } else {
                            let play_button =
                                ui.add(egui::Button::new("▶").min_size(min_button_size));
                            if play_button.clicked() {
                                auto_advance.0 = true;
                            };
                        }
                    });

                    ui.end_row();

                    ui.horizontal(|ui| {
                        let total_duration =
                            Duration::from_years(runtime_parameters.duration as f32);
                        let simulated_duration = Duration::new(
                            &simulation_parameters.beginning(&runtime_parameters),
                            &date.0,
                        );
                        let progress = simulated_duration / total_duration;
                        ui.add(
                            ProgressBar::new(progress)
                                .text(date.0.year().to_string())
                                .desired_width(size.x)
                                .desired_height(12.0),
                        );
                    });
                });
        });
}

fn paint_replay_controls(
    mut contexts: EguiContexts,
    history: Res<History>,
    snapshots: Res<Snapshots>,
    simulation_parameters: Res<SimulationParameters>,
    mut slider_value: ResMut<DateSliderValue>,
    mut time: ResMut<Time<Virtual>>,
) {
    egui::TopBottomPanel::bottom("explore_controls")
        .show_separator_line(false)
        .frame(Frame {
            inner_margin: Margin {
                bottom: 20.,
                top: 0.,
                left: 40.,
                right: 40.,
            },
            shadow: Shadow::NONE,
            stroke: Stroke::NONE,
            ..default()
        })
        .show(contexts.ctx_mut(), |ui| {
            let size = ui.available_size();
            ui.spacing_mut().slider_width = size.x;
            egui::Grid::new("playback_controls_grid").show(ui, |ui| {
                ui.horizontal(|ui| {
                    let min_button_size = egui::Vec2::new(32., 26.);

                    let previous_button = ui.add(egui::Button::new("⏮").min_size(min_button_size));
                    if previous_button.clicked() {
                        slider_value.0 = (slider_value.0 - 0.3).floor();
                    }

                    if time.is_paused() || time.relative_speed() != 1.0 {
                        let play_button = ui.add(egui::Button::new("▶").min_size(min_button_size));
                        if play_button.clicked() {
                            time.unpause();
                            time.set_relative_speed(1.0);
                        };
                    } else {
                        let pause_button = ui.add(egui::Button::new("⏸").min_size(min_button_size));
                        if pause_button.clicked() {
                            time.pause();
                        }
                    }

                    let next_speed = (time.relative_speed() * 2.0).min(128.0);
                    let label = format!("⏩ ×{next_speed:.0}");
                    let speedup_button = ui.add(egui::Button::new(label).min_size(min_button_size));
                    if speedup_button.clicked() {
                        time.unpause();
                        time.set_relative_speed(next_speed);
                    }

                    let next_button = ui.add(egui::Button::new("⏭").min_size(min_button_size));
                    if next_button.clicked() {
                        slider_value.0 = (slider_value.0 + 0.3).ceil();
                    }

                    let export_button = ui.add(egui::Button::new("⏏").min_size(min_button_size));
                    if export_button.clicked() {
                        // TODO: Can I avoid cloning data to save a simulation?
                        //       Saving needs read-only access, so it should be possible.
                        ron_export::export(history.clone(), simulation_parameters.clone());
                    }
                });

                ui.end_row();

                let snapshots_count = (snapshots.collection.len() - 1) as f64;
                let range = 0.0..=snapshots_count;

                let slider = Slider::from_get_set(range, |input| {
                    if let Some(value) = input {
                        slider_value.0 = value;
                        value
                    } else {
                        slider_value.0
                    }
                })
                .show_value(false)
                .trailing_fill(true)
                .handle_shape(egui::style::HandleShape::Rect { aspect_ratio: 0.5 })
                // NOTE: There is an implicit assumption here that snapshots are taken every month
                .integer();
                ui.add(slider);
            })
        });
}

fn update_slider_value(
    mut slider_value: ResMut<DateSliderValue>,
    date: Res<Date>,
    history: Res<History>,
) {
    let beginning = history.events.first().unwrap().date;
    let duration = Duration::new(&beginning, &date.0);
    slider_value.bypass_change_detection().0 = duration.months as f64;
}

fn apply_slider_value(
    slider_value: ResMut<DateSliderValue>,
    snapshots: Res<Snapshots>,
    systems: Res<HistorySystems>,
    mut commands: Commands,
) {
    let index = slider_value.0 as usize;
    debug!(
        "Loading snapshot {index} out of {count}",
        count = snapshots.collection.len()
    );
    let Some(snapshot) = snapshots.collection.get(index) else {
        warn!("No snapshot at index {index}!");
        return;
    };

    commands.run_system_with_input(systems.rollback, snapshot.clone());
}

fn paint_population_panel(
    mut contexts: EguiContexts,
    persons: Query<
        (
            &PersonId,
            &Name,
            &Sex,
            &BirthDate,
            &Savings,
            Option<&Residence>,
        ),
        With<Person>,
    >,
    buildings: Query<&Name, With<Building>>,
    persons_register: Res<PersonsRegister>,
    buildings_register: Res<BuildingsRegister>,
    selecting_systems: Res<SelectingSystems>,
    mut commands: Commands,
    date: Res<Date>,
) {
    egui::CentralPanel::default()
        .frame(Frame {
            inner_margin: Margin::symmetric(40., 20.),
            outer_margin: Margin {
                top: 60.,
                left: 40.,
                right: 40.,
                bottom: 10.,
            },
            shadow: Shadow::NONE,
            stroke: Stroke::NONE,
            fill: Color32::from_rgba_premultiplied(20, 20, 20, 240),
            ..default()
        })
        .show(contexts.ctx_mut(), |ui| {
            let count = persons.into_iter().len();
            let registered = persons_register.0.len();
            ui.heading(format!("The {count} ({registered}) People of Otterhide"));
            egui::ScrollArea::both().show(ui, |ui| {
                // TODO: Use Table from egui_extras
                egui::Grid::new("persons-grid").show(ui, |ui| {
                    ui.label("Name");
                    ui.label("Sex");
                    ui.label("Age");
                    ui.label("Savings");
                    ui.label("Address");
                    ui.label("Id");
                    ui.end_row();

                    for (id, name, sex, BirthDate(birth_date), Savings(savings), residence) in
                        persons.iter()
                    {
                        // Name
                        if ui.link(name.to_string()).clicked() {
                            commands
                                .run_system_with_input(selecting_systems.select, id.clone().into())
                        }


                        let age = Duration::new(birth_date, date.0.borrow()).years();

                        ui.label(sex.to_string());
                        ui.label(age.to_string());
                        ui.label(format!("{savings:.2} Œ"));

                        // Address
                        if let Some(Residence(residence_id)) = residence {
                            if let Some(building_entity) = buildings_register.0.get(residence_id) {
                                match buildings.get(*building_entity) {
                                    Ok(address) => {
                                        if ui.link(address.as_str()).clicked() {
                                            commands.run_system_with_input(
                                                selecting_systems.select,
                                                residence_id.clone().into(),
                                            );
                                        }
                                    },
                                    Err(error) => {
                                        ui_error!(ui, "Can't find {residence_id:?} among buildings: {error:#?}");
                                    },
                                }
                            } else {
                                ui_error!(ui, "Can't find {residence_id:?} in the buildings register.");
                            }
                        } else {
                            ui.label(RichText::new("No known address").italics());
                        }
                        if ui.link(id.0.to_string()).clicked() {
                            commands
                                .run_system_with_input(selecting_systems.select, id.clone().into())
                        }
                        ui.end_row();
                    }
                })
            })
        });
}

fn paint_buildings_panel(
    mut contexts: EguiContexts,
    buildings: Query<
        (
            &BuildingId,
            &Name,
            &BuildingClass,
            &HousingCapacity,
            Option<&Residents>,
        ),
        With<Building>,
    >,
    persons: Query<(&Name, &BirthDate, &Sex), With<Person>>,
    buildings_register: Res<BuildingsRegister>,
    persons_register: Res<PersonsRegister>,
    selecting_systems: Res<SelectingSystems>,
    mut commands: Commands,
    date: Res<Date>,
) {
    // TODO: DRY on inspector panels layout ...
    egui::CentralPanel::default()
        .frame(Frame {
            inner_margin: Margin::symmetric(40., 20.),
            outer_margin: Margin {
                top: 60.,
                left: 40.,
                right: 40.,
                bottom: 10.,
            },
            shadow: Shadow::NONE,
            stroke: Stroke::NONE,
            fill: Color32::from_rgba_premultiplied(20, 20, 20, 240),
            ..default()
        })
        .show(contexts.ctx_mut(), |ui| {
            // ... Down to here it should be it's own function
            let count = buildings.into_iter().len();
            let registered = buildings_register.0.len();
            ui.heading(format!("The {count} ({registered}) Buildings of Otterhide"));
            egui::ScrollArea::both().show(ui, |ui| {
                // TODO: Use Table from egui_extras
                egui::Grid::new("persons-grid")
                    .striped(true)
                    .show(ui, |ui| {
                        ui.label("Address");
                        ui.label("Class");
                        ui.label("Capacity");
                        ui.label("Residents:");
                        ui.label("Id");
                        ui.end_row();

                        for (
                            id,
                            address,
                            BuildingClass(class),
                            HousingCapacity(capacity),
                            residents,
                        ) in buildings.iter()
                        {


                            if ui.link(address.to_string()).clicked() {
                                commands.run_system_with_input(
                                    selecting_systems.select,
                                    id.clone().into(),
                                )
                            }

                            ui.label(class);

                            // Residents
                            if let Some(Residents(residents)) = residents {
                                ui.with_layout(Layout::top_down(Align::TOP), |ui| {

                                    for resident_id in residents {
                                        if let Some(resident_entity) = persons_register.0.get(resident_id) {
                                            if let Ok((name, BirthDate(birth_date), sex)) = persons.get(*resident_entity) {
                                                let age = Duration::new(&birth_date, &date.0).years();
                                                let text = format!("{sex} {age:02} {name}");
                                                if ui.link(text).clicked() {
                                                    commands.run_system_with_input(selecting_systems.select, resident_id.clone().into());
                                                }
                                            } else {
                                                ui_error!(ui, "Can't find {resident_id:?} among persons");
                                            }
                                        } else {
                                            ui_error!(ui, "Can't find {resident_id:?} in the persons register.");
                                        }
                                    }

                                });
                            } else {
                                ui.label(RichText::new("No residents").italics());
                            };

                            ui.label(capacity.to_string());

                            // Id
                            if ui.link(id.0.to_string()).clicked() {
                                commands.run_system_with_input(
                                    selecting_systems.select,
                                    id.clone().into(),
                                )
                            }
                            ui.end_row();
                        }
                        })
            })
        });
}
