use crate::heads_up_display::InspectorPanel;
use bevy::prelude::*;
use bevy_args::{BevyArgsPlugin, Deserialize, Parser, Serialize};

pub struct ConfigurationPlugin;

impl Plugin for ConfigurationPlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<Configuration>()
            .add_plugins(BevyArgsPlugin::<Configuration>::default())
            .add_systems(Startup, print_configuration);
    }
}

const DEFAULT_BEGINNING: u16 = 1840;

#[cfg(debug_assertions)]
const DEFAULT_DURATION: u16 = 10;
#[cfg(not(debug_assertions))]
const DEFAULT_DURATION: u16 = 150;

// In web browsers the scrolling is reported differently than on desktop
//
// Those defaults work for my browser and laptop.
#[cfg(not(target_arch = "wasm32"))]
const DEFAULT_VERTICAL_SCROLL_SENSITIVITY: f32 = 1.0;
#[cfg(not(target_arch = "wasm32"))]
const DEFAULT_HORIZONTAL_SCROLL_SENSITIVITY: f32 = 1.0;
#[cfg(target_arch = "wasm32")]
const DEFAULT_VERTICAL_SCROLL_SENSITIVITY: f32 = 0.2;
#[cfg(target_arch = "wasm32")]
const DEFAULT_HORIZONTAL_SCROLL_SENSITIVITY: f32 = 0.2;

#[derive(Resource, Clone, Debug, Serialize, Deserialize, Parser, Reflect)]
#[reflect(Resource)]
#[serde(default)]
#[command(about = "Otterhide town simulator", version)]
pub struct Configuration {
    /// Simulation .ron file path relative to assets/ directory
    #[arg(long)]
    pub load: Option<String>,

    /// Which inspection panel to display if any
    #[arg(long)]
    pub inspect: Option<InspectorPanel>,

    /// Number of years to simulate
    #[arg(long, default_value_t = DEFAULT_DURATION)]
    pub duration: u16,

    /// First year of the simulation
    #[arg(long, default_value_t = DEFAULT_BEGINNING)]
    pub beginning: u16,

    /// Should gizmos be drawn for visual debugging
    #[arg(long, default_value_t = false)]
    pub gizmos: bool,

    /// Vertical scroll sensitivity
    #[arg(long, default_value_t = DEFAULT_VERTICAL_SCROLL_SENSITIVITY)]
    pub vertical_scroll_sensitivity: f32,

    /// Horizontal scroll sensitivity
    #[arg(long, default_value_t = DEFAULT_HORIZONTAL_SCROLL_SENSITIVITY)]
    pub horizontal_scroll_sensitivity: f32,
}

impl Default for Configuration {
    fn default() -> Self {
        Self {
            beginning: DEFAULT_BEGINNING,
            duration: DEFAULT_DURATION,
            inspect: None,
            load: None,
            gizmos: false,
            vertical_scroll_sensitivity: DEFAULT_VERTICAL_SCROLL_SENSITIVITY,
            horizontal_scroll_sensitivity: DEFAULT_HORIZONTAL_SCROLL_SENSITIVITY,
        }
    }
}

fn print_configuration(configuration: Res<Configuration>) {
    info!("{configuration:#?}")
}
