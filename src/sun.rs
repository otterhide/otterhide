use crate::date::Date;
use crate::major_state::MajorState;
use crate::simulation;
use crate::simulation::SimulationParameters;
use bevy::prelude::*;
use std::f32::consts::PI;
use std::ops::{Div, Mul, Sub};

pub struct SunPlugin;

impl Plugin for SunPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            OnEnter(MajorState::GettingReady),
            setup_sunlight.after(simulation::get_ready),
        )
        .add_systems(
            Update,
            (move_sun, update_sunlight_color, draw_gizmos).run_if(resource_exists::<Date>),
        );
    }
}

#[derive(Component)]
struct Sun;

fn draw_gizmos(mut gizmos: Gizmos, sun: Query<(&Transform, &DirectionalLight), With<Sun>>) {
    let (position, light) = sun.single();
    gizmos.sphere(position.translation, Quat::default(), 100., light.color);
}

fn setup_sunlight(mut commands: Commands, settings: Res<SimulationParameters>) {
    let orbit = settings.land_radius + settings.sun_gap;

    commands
        .spawn(DirectionalLightBundle {
            transform: Transform::from_translation(Vec3::NEG_Y * orbit)
                .looking_at(Vec3::ZERO, Vec3::Y),
            directional_light: DirectionalLight {
                shadows_enabled: true,
                color: Color::hsl(0.0, 1.0, 0.9),
                ..default()
            },
            ..default()
        })
        .insert(Sun);
}

fn move_sun(
    mut sun: Query<&mut Transform, With<Sun>>,
    date: Res<Date>,
    settings: Res<SimulationParameters>,
    state: Res<State<MajorState>>,
) {
    // TODO: Take elevation into account, so that the gap is actually between the edge of land and the sun
    let orbit = settings.land_radius + settings.sun_gap;

    let angle = if *state == MajorState::Exploring {
        // In Exploration state sun goes around every day-month
        let daytime = date.0.time_of_day();
        daytime * 2.0 * PI - PI
    } else {
        // In simulation or getting ready, sun goes around every year
        // In those states time progresses on rapidly (as fast as possible), so
        // day-monthly sun cycle would look like a stroboscope.
        let season = (date.0.month() as usize as f32 + date.0.time_of_day())
            .rem_euclid(12.0)
            .div(12.0);
        season * 2.0 * PI - PI
    };
    // TODO: Lean the orbit more in winters.
    let rotation = Quat::from_rotation_x(angle) * Quat::from_rotation_z(1.0);
    let translation = rotation.mul_vec3(Vec3::Y) * orbit;

    let mut transform = sun.single_mut();
    transform.translation = translation + Vec3::Y * settings.sun_elevation;
    transform.look_at(Vec3::ZERO, Vec3::Y);
}

fn update_sunlight_color(
    mut sun: Query<&mut DirectionalLight, With<Sun>>,
    date: Res<Date>,

    state: Res<State<MajorState>>,
) {
    let daytime = if *state == MajorState::Exploring {
        date.0.time_of_day()
    } else {
        (date.0.month() as usize as f32 + date.0.time_of_day())
            .rem_euclid(12.0)
            .div(12.0)
    };
    let sine = daytime.sub(0.25).mul(2.0 * PI).sin();
    let hue = 10.0 + (sine + 0.5) * 30.0;
    let lightness = (sine + 1.0) * 0.1 + 0.75; // between 0.75 and 0.95
    let illuminance = (sine + 0.5) / 2.0 * 32_000.0;

    let mut sun = sun.single_mut();
    sun.color.set_h(hue);
    sun.color.set_l(lightness);
    sun.illuminance = illuminance.max(0.0);
}

// TODO: Add ambient light. As it is, at noon shadows are too dark.
