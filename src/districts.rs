use crate::buildings::Building;
use crate::coordinates::{Coordinate, Coordinates, Direction, Latitude, Longitude};
use crate::date::SimulationFrameCounter;
use crate::ground::GroundSystems;
use crate::history::Snapshot;
use crate::major_state::MajorState;
use crate::major_state::PreloadedAssets;
use crate::names::{DISTRICT_NAMES, DISTRICT_PREFIXES};
use crate::parcels::{setup_parcels, Parcel};
use crate::population::Person;
use crate::roads::{RoadPlan, RoadsSystems};
use crate::simulation::SimulationParameters;
use bevy::ecs::system::{RunSystemOnce, SystemId};
use bevy::gltf::Gltf;
use bevy::math::bounding::Aabb3d;
use bevy::prelude::*;
use bevy::utils::{HashMap, HashSet};
use bevy_rts_camera::Ground;
use itertools::Itertools;
use rand::seq::{IteratorRandom, SliceRandom};
use rand::thread_rng;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::f32::consts::FRAC_PI_2;
use std::iter::{self, once};
use std::ops::{AddAssign, Not};
use std::str::FromStr;

pub struct DistrictsPlugin;

impl Plugin for DistrictsPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<NewDistrictEstablished>()
            .init_resource::<DistrictScenes>()
            .add_systems(Startup, register_district_systems)
            .add_systems(Startup, setup_assets)
            .add_systems(OnEnter(MajorState::GettingReady), setup_districts)
            .add_systems(
                Update,
                plan_new_districts.after(implement_new_districts).run_if(
                    in_state(MajorState::Simulating)
                        .and_then(resource_changed::<SimulationFrameCounter>),
                ),
            )
            // TODO: Not in the getting ready state
            .add_systems(Update, implement_new_districts);
    }
}

/// Remembers where to find the districts GLTF asset among all the other
#[derive(Resource)]
struct DistrictsAssetHandle(Handle<Gltf>);

fn setup_assets(
    assets: Res<AssetServer>,
    mut commands: Commands,
    mut preloaded: ResMut<PreloadedAssets>,
) {
    const ASSET_PATH: &str = "districts.glb";
    info!("Loading {ASSET_PATH} asset");
    let handle = assets.load(ASSET_PATH);
    preloaded.0.insert(handle.clone().untyped());
    commands.insert_resource(DistrictsAssetHandle(handle));
}

#[cfg(test)]
mod district_variant_tests {
    use super::*;

    #[test]
    fn valid_name() {
        let name = "district-180x300.002";
        let expected = Ok(DistrictVariant {
            length: 18,
            width: 30,
            number: 2,
        });
        let actual = name.parse();
        assert_eq!(actual, expected);
    }

    #[test]
    fn invalid_name() {
        // Negatives - bad
        let name = "district-10x-20-qux";
        let expected: Result<DistrictVariant, _> =
            Err(DistrictVariantError::NotMatchingPattern(name.to_string()));
        let actual = name.parse();
        assert_eq!(actual, expected);

        // Not a number - bad
        let name = "district-barxbaz-qux";
        let expected: Result<DistrictVariant, _> =
            Err(DistrictVariantError::NotMatchingPattern(name.to_string()));
        let actual = name.parse();
        assert_eq!(actual, expected);

        // Not divisible by 10 - bad
        let name = "district-200x205-qux";
        let expected: Result<DistrictVariant, _> =
            Err(DistrictVariantError::NotMatchingPattern(name.to_string()));
        let actual = name.parse();
        assert_eq!(actual, expected);
    }
}

fn setup_districts(
    districts_asset_handle: Res<DistrictsAssetHandle>,
    gltf_assets: Res<Assets<Gltf>>,
    mut scenes_assets: ResMut<Assets<Scene>>,
    mut scenes: ResMut<DistrictScenes>,
) {
    let districts_gltf = gltf_assets.get(&districts_asset_handle.0).unwrap();

    for (name, scene_handle) in districts_gltf.named_scenes.iter() {
        debug!("Processing district {name}");
        let Ok(variant) = name.parse::<DistrictVariant>() else {
            panic!("The districts asset contains a scene that can't be parsed into a DistrictVariant: {name}");
        };

        let scene = scenes_assets.get_mut(scene_handle.clone()).unwrap();

        scene.world.run_system_once(mark_ground);
        // TODO: Use run_system_once call pattern to setup parcels
        setup_parcels(scene);

        scenes.0.insert(variant, scene_handle.clone());
    }

    let variants = scenes
        .0
        .keys()
        .map(|variant| format!("  - {variant:?}"))
        .join("\n");
    info!("Districts processed: \n{variants}");
}

fn mark_ground(objects: Query<(Entity, &Name)>, mut commands: Commands) {
    for (entity, name) in objects.iter() {
        let name = &name.as_str();
        if name.starts_with("District ground") || name.starts_with("Walkway") {
            info!("Marking entity {entity:?} (named `{name}`) as Ground");
            commands.entity(entity).insert(Ground);
        }
    }
}

#[derive(Bundle)]
struct DistrictBundle {
    marker: District,
    /// The logical description of the district, used for snapshots
    description: DistrictDescription,
    /// Do not set the scene directly. it is derived from the description.
    scene: SceneBundle,
    name: Name,
}

impl DistrictBundle {
    fn new(description: DistrictDescription, scenes: &DistrictScenes) -> Self {
        let variant = &description.variant;
        let scene = SceneBundle {
            scene: scenes.0.get(variant).unwrap().clone(),
            transform: description.borrow().into(),
            ..default()
        };
        Self {
            name: description.name.clone(),
            description,
            scene,
            marker: District,
        }
    }
}

/// A marker for district entities

#[derive(Component, Debug, Clone, Copy)]
pub struct District;

#[derive(Debug, Clone, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct DistrictVariant {
    /// The length of the district model
    ///
    /// Be careful when reading this value in a context of a district
    /// description, as it's rotation may swap original length and width. Better
    /// use the DistrictDescription::length method.
    length: u32,

    /// The width of the district model
    ///
    /// Be careful when reading this value in a context of a district
    /// description, as it's rotation may swap original length and width. Better
    /// use the DistrictDescription::width method.
    width: u32,

    /// The numerical value to distinguish between variants with the same size
    ///
    /// The .001, .002, etc. part in Blender's scene name. For example it will
    /// be 2 in «district-180x300.002». It doesn't have any semantic meaning,
    /// other than to distinguish between otherwise equivalent building
    /// variants.
    number: u16,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DistrictVariantError {
    NotMatchingPattern(String),
    InvalidLength(String),
    InvalidWidth(String),
    InvalidNumber(String),
}

impl FromStr for DistrictVariant {
    type Err = DistrictVariantError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // If pattern is invalid, it's not recoverable.
        // TODO: Make invalid pattern a compilation error.
        // NOTE: Pattern assumes length and width end in 0
        // They must be divisible by 10 meters. Only the preceding digits are
        // captured, effectively dividing the input by 10, so it matches units
        // of coordinates. It's a hack. Ugly?
        let pattern =
            Regex::new(r"^district-(?<length>\d+)0x(?<width>\d+)0\.(?<number>\d+)$").unwrap();
        let Some(captures) = pattern.captures(s) else {
            return Err(DistrictVariantError::NotMatchingPattern(s.to_string()));
        };

        // Extract captured groups
        let length = captures["length"].to_string();
        let width = captures["width"].to_string();
        let number = captures["number"].to_string();

        // Parse
        let Ok(length) = length.parse::<u32>() else {
            return Err(DistrictVariantError::InvalidLength(length));
        };
        let Ok(width) = width.parse::<u32>() else {
            return Err(DistrictVariantError::InvalidWidth(width));
        };
        let Ok(number) = number.parse::<u16>() else {
            return Err(DistrictVariantError::InvalidNumber(number));
        };

        Ok(DistrictVariant {
            length,
            width,
            number,
        })
    }
}

#[derive(Resource, Debug, Default)]
struct DistrictScenes(HashMap<DistrictVariant, Handle<Scene>>);

/// This event represents a decision to construct a new building.
///
/// It will be stored in the history, and will result in spawning a new district.
#[derive(Event, Clone, Debug, Serialize, Deserialize)]
pub struct NewDistrictEstablished(pub DistrictDescription);

// TODO: Divide into smaller components.
/// A logical description of the district, used for planning and snapshotting
#[derive(Component, Debug, Clone, Serialize, Deserialize)]
pub struct DistrictDescription {
    /// Coordinates of the north-west corner, after the rotation
    pub origin: Coordinates,

    /// How is the district rotated compared to the model
    pub rotation: Rotation,

    /// Which model to use
    pub variant: DistrictVariant,

    /// The name of this district
    pub name: Name,
}

impl From<&DistrictDescription> for Aabb3d {
    fn from(value: &DistrictDescription) -> Self {
        Self::new(
            value.center(),
            Vec3 {
                x: (value.length() * 5) as f32,
                y: 30.0,
                z: (value.width() * 5) as f32,
            },
        )
    }
}

#[derive(Component, Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Rotation {
    None = 0,
    CW90,
    CW180,
    CW270,
}

impl AddAssign for Rotation {
    fn add_assign(&mut self, rhs: Self) {
        let current = *self as isize;
        let increase = rhs as isize;
        *self = Self::from(current + increase);
    }
}

impl From<Rotation> for Quat {
    fn from(value: Rotation) -> Self {
        let angle = value as isize as f32 * FRAC_PI_2;

        // TODO: Seems like Quat::from_axis_angle is counter-clockwise. Shall the Rotation be too?
        Quat::from_axis_angle(Vec3::Y, -angle)
    }
}

impl From<isize> for Rotation {
    fn from(value: isize) -> Self {
        let remainder = value.rem_euclid(4);
        match remainder {
            0 => Rotation::None,
            1 => Rotation::CW90,
            2 => Rotation::CW180,
            3 => Rotation::CW270,
            _ => {
                panic!("The remainder of {value} modulo 4 = {remainder} (not in 0 - 3 range). WTF?")
            }
        }
    }
}

impl DistrictDescription {
    pub fn new(name: String, origin: Coordinates, variant: DistrictVariant) -> Self {
        Self {
            name: Name::new(name),
            origin,
            variant,
            rotation: Rotation::None,
        }
    }

    pub fn shift(&mut self, distance: i32, direction: &Direction) -> &mut Self {
        self.origin.shift(distance, direction);
        self
    }

    // TODO: Consider removing District::rotate, since it's just an add_assign on rotation
    pub fn rotate(&mut self, rotation: Rotation) -> &mut Self {
        self.rotation += rotation;
        self
    }

    pub fn plan_border_roads(&self) -> RoadPlan {
        RoadPlan::default()
            .add_road(&self.north_west(), &Direction::East, self.length())
            .add_road(&self.north_east(), &Direction::South, self.width())
            .add_road(&self.south_east(), &Direction::West, self.length())
            .add_road(&self.south_west(), &Direction::North, self.width())
            .to_owned()
    }

    fn overlaps(&self, other: &Self) -> bool {
        Rect::from(self).intersect(other.into()).is_empty().not()
    }

    pub fn center(&self) -> Vec3 {
        let Vec2 { x, y } = Rect::from(self).center();
        Vec3 { x, y: 0.0, z: y }
    }

    // Edges

    fn south(&self) -> Coordinate<Latitude> {
        self.origin.latitude() + (self.width() as i32)
    }

    fn east(&self) -> Coordinate<Longitude> {
        self.origin.longitude() + (self.length() as i32)
    }

    fn north(&self) -> Coordinate<Latitude> {
        self.origin.latitude()
    }

    fn west(&self) -> Coordinate<Longitude> {
        self.origin.longitude()
    }

    // Corners

    fn corners(&self) -> [Coordinates; 4] {
        [
            self.north_east(),
            self.south_east(),
            self.south_west(),
            self.north_west(),
        ]
    }

    fn north_east(&self) -> Coordinates {
        *self
            .origin
            .clone()
            .shift(self.length() as i32, &Direction::East)
    }

    fn north_west(&self) -> Coordinates {
        self.origin
    }

    fn south_east(&self) -> Coordinates {
        *self
            .origin
            .clone()
            .shift(self.width() as i32, &Direction::South)
            .shift(self.length() as i32, &Direction::East)
    }

    fn south_west(&self) -> Coordinates {
        *self
            .origin
            .clone()
            .shift(self.width() as i32, &Direction::South)
    }

    // Dimensions

    /// Dimension from the east to the west edge (along the longitude)
    ///
    /// Depends on rotation and variant.
    fn length(&self) -> u32 {
        match self.rotation {
            Rotation::None => self.variant.length,
            Rotation::CW90 => self.variant.width,
            Rotation::CW180 => self.variant.length,
            Rotation::CW270 => self.variant.width,
        }
    }

    /// Dimension from the north to the south edge (along the latitude)
    fn width(&self) -> u32 {
        match self.rotation {
            Rotation::None => self.variant.width,
            Rotation::CW90 => self.variant.length,
            Rotation::CW180 => self.variant.width,
            Rotation::CW270 => self.variant.length,
        }
    }
}

impl From<&DistrictDescription> for Rect {
    fn from(value: &DistrictDescription) -> Self {
        Rect::from_corners(
            value.north_west().borrow().into(),
            value.south_east().borrow().into(),
        )
    }
}

impl From<&DistrictDescription> for Transform {
    fn from(description: &DistrictDescription) -> Self {
        let alignment: Transform = match description.rotation {
            Rotation::None => Transform::default(),
            Rotation::CW90 => Transform::from_translation(Vec3 {
                z: (description.length() * 10) as f32 * -1.0,
                ..default()
            }),
            Rotation::CW180 => Transform::from_translation(Vec3 {
                x: (description.length() * 10) as f32 * -1.0,
                z: (description.width() * 10) as f32 * -1.0,
                ..default()
            }),
            Rotation::CW270 => Transform::from_translation(Vec3 {
                x: (description.width() * 10) as f32 * -1.0,
                ..default()
            }),
        };
        Transform::from_translation(description.origin.borrow().into())
            .with_rotation(description.rotation.into())
            .mul_transform(alignment)
    }
}

fn plan_new_districts(
    districts: Query<&DistrictDescription>,
    settings: Res<SimulationParameters>,
    people: Query<&Person>,
    parcels: Query<&Parcel>,
    buildings: Query<&Building>,
    scenes: Res<DistrictScenes>,
    mut new_districts: EventWriter<NewDistrictEstablished>,
) {
    let population = people.iter().count();
    let capacity = buildings.iter().count() + parcels.iter().count();

    if population < capacity {
        return;
    }
    debug!("Not enough living space ({population} / {capacity}). Planning a new district.");

    // TODO: Use some smarter heuristics to choose the district
    let variant = scenes.0.keys().choose(&mut thread_rng()).unwrap();
    let Some(name) = random_district_name(&districts) else {
        warn!("Couldn't find a name for a new district. It might work next time.");
        return;
    };

    // TODO: Ensure names are unique

    let corners = districts
        .iter()
        .flat_map(|district| district.corners().into_iter())
        .chain(iter::once(Coordinates::default()))
        .unique();

    let candidates = corners
        .map(|corner| DistrictDescription::new(name.to_string(), corner, variant.clone()))
        .flat_map(|candidate| {
            [
                candidate.clone().rotate(Rotation::None).to_owned(),
                candidate.clone().rotate(Rotation::CW90).to_owned(),
                candidate.clone().rotate(Rotation::CW180).to_owned(),
                candidate.clone().rotate(Rotation::CW270).to_owned(),
            ]
            .into_iter()
        })
        .flat_map(|candidate| {
            [
                candidate.clone(),
                candidate
                    .clone()
                    .shift(candidate.width() as i32, &Direction::North)
                    .to_owned(),
                candidate
                    .clone()
                    .shift(candidate.width() as i32, &Direction::North)
                    .shift(candidate.length() as i32, &Direction::West)
                    .to_owned(),
                candidate
                    .clone()
                    .shift(candidate.length() as i32, &Direction::West)
                    .to_owned(),
            ]
            .into_iter()
        })
        .filter(|candidate| {
            debug!("Trying to place a district {candidate:?}");

            // TODO: Extract into Latitude::same_hemisphere method
            if i32::from(&candidate.east()) * i32::from(&candidate.west()) < 0 {
                debug!("District would cross the latitudinal highway");
                return false;
            }
            if i32::from(&candidate.north()) * i32::from(&candidate.south()) < 0 {
                debug!("District would cross the longitudinal highway");
                return false;
            }

            let outer_buffer = ((candidate.length() + candidate.width()) * 10) as f32;
            let max_distance = settings.land_radius - outer_buffer;
            if candidate.center().length() > max_distance {
                debug!("District would be too close to the edge.");
                return false;
            }

            districts
                .iter()
                .any(|existing| existing.overlaps(candidate))
                .not()
        });

    let Some(selected) = candidates.choose(&mut thread_rng()) else {
        debug!("Can't find a suitable spot for a new district!");
        return;
    };

    debug!("New district established: {selected:#?}");
    new_districts.send(NewDistrictEstablished(selected));
}

/// Get a random, unique name for a new district
///
/// NOTE: This function has a minuscule chance of returning None, even when
///       there are possible names. It's a feature?
fn random_district_name(districts: &Query<&DistrictDescription>) -> Option<String> {
    let used_names: HashSet<String> = districts
        .iter()
        .map(|district| district.name.to_string())
        .collect();
    let base_name = DISTRICT_NAMES.iter().choose(&mut thread_rng()).unwrap();
    let mut prefixes = DISTRICT_PREFIXES.to_owned();
    prefixes.shuffle(&mut thread_rng());
    let variations = prefixes
        .iter()
        .map(|prefix| format!("{prefix} {base_name}"));
    let mut candidates = once(base_name.to_string()).chain(variations);
    candidates.find(|name| used_names.contains(name).not())
}

/// Process NewDistrictEstablished events
///
/// The events might be coming from establish_new_districts or replay_historical_events
fn implement_new_districts(
    mut commands: Commands,
    mut new_districts: EventReader<NewDistrictEstablished>,
    systems: Res<DistrictsSystems>,
) {
    for NewDistrictEstablished(district) in new_districts.read() {
        commands.run_system_with_input(systems.construct_district, district.clone());
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DistrictsSnapshot(Vec<DistrictDescription>);

impl Snapshot for DistrictsSnapshot {
    fn capture(world: &mut World) -> Self {
        let mut districts = world.query::<&DistrictDescription>();
        Self(districts.iter(world).cloned().collect())
    }

    fn restore(&self, world: &mut World) {
        let mut districts = world.query_filtered::<Entity, With<DistrictDescription>>();
        let entities = districts.iter(world).collect_vec();
        for entity in entities {
            despawn_with_children_recursive(world, entity);
        }

        let construct_district =
            world.resource_scope(|_, systems: Mut<DistrictsSystems>| systems.construct_district);
        for district in self.0.iter() {
            world
                .run_system_with_input(construct_district, district.to_owned())
                .unwrap();
        }
    }
}

// Systems exposed to other systems

#[derive(Resource, Debug)]
pub struct DistrictsSystems {
    pub construct_district: SystemId<DistrictDescription>,
}

fn register_district_systems(world: &mut World) {
    let construct_district = world.register_system(construct_district);

    world.insert_resource(DistrictsSystems { construct_district });
}

fn construct_district(
    In(description): In<DistrictDescription>,
    scenes: Res<DistrictScenes>,
    roads_systems: Res<RoadsSystems>,
    ground_systems: Res<GroundSystems>,
    mut commands: Commands,
) {
    debug!("Spawning a district: {description:#?}");

    commands.run_system_with_input(
        roads_systems.implement_road_plan,
        description.plan_border_roads(),
    );

    let bound = Aabb3d::from(&description);
    commands.run_system_with_input(ground_systems.remove_obstacles, bound);

    commands.spawn(DistrictBundle::new(description, &scenes));
}

#[cfg(test)]
mod district_tests {
    use super::*;
    use bevy::math::Rect;
    use std::ops::Not;

    #[test]
    fn measurements_test() {
        let district = DistrictDescription::new(
            "a".into(),
            Coordinates::new(Coordinate::new(-3), Coordinate::new(-2)),
            DistrictVariant {
                length: 13,
                width: 7,
                number: 0,
            },
        );

        assert_eq!(district.east(), Coordinate::new(10));
        assert_eq!(district.west(), Coordinate::new(-3));
        assert_eq!(district.north(), Coordinate::new(-2));
        assert_eq!(district.south(), Coordinate::new(5));
        assert_eq!(district.length(), 13);
        assert_eq!(district.width(), 7);
        assert_eq!(
            district.north_east(),
            Coordinates::new(
                Coordinate::<Longitude>::new(10),
                Coordinate::<Latitude>::new(-2)
            )
        );
        assert_eq!(
            district.north_west(),
            Coordinates::new(
                Coordinate::<Longitude>::new(-3),
                Coordinate::<Latitude>::new(-2)
            )
        );
        assert_eq!(
            district.south_east(),
            Coordinates::new(
                Coordinate::<Longitude>::new(10),
                Coordinate::<Latitude>::new(5)
            )
        );
        assert_eq!(
            district.south_west(),
            Coordinates::new(
                Coordinate::<Longitude>::new(-3),
                Coordinate::<Latitude>::new(5)
            )
        );
    }

    #[test]
    fn into_rect() {
        let variant = DistrictVariant {
            length: 10,
            width: 20,
            number: 0,
        };
        let district = DistrictDescription::new("a".into(), Coordinates::default(), variant);
        let rect = Rect::from(&district);
        assert_eq!(rect.min, Vec2::ZERO);
        assert_eq!(rect.max, Vec2::new(100.0, 200.0));

        let variant = DistrictVariant {
            length: 10,
            width: 20,
            number: 0,
        };
        let district = DistrictDescription::new(
            "b".into(),
            Coordinates::new(
                Coordinate::<Longitude>::new(10),
                Coordinate::<Latitude>::new(5),
            ),
            variant,
        );
        let rect = Rect::from(&district);
        assert_eq!(rect.min, Vec2::new(100.0, 50.0));
        assert_eq!(rect.max, Vec2::new(200.0, 250.0));
    }

    #[test]
    fn center() {
        let variant = DistrictVariant {
            length: 10,
            width: 20,
            number: 0,
        };
        let district = DistrictDescription::new("a".into(), Coordinates::default(), variant);
        assert_eq!(district.center(), Vec3::new(50.0, 0.0, 100.0));

        let variant = DistrictVariant {
            length: 10,
            width: 15,
            number: 0,
        };
        let district = DistrictDescription::new(
            "a".into(),
            Coordinates::new(
                Coordinate::<Longitude>::new(10),
                Coordinate::<Latitude>::new(5),
            ),
            variant,
        );
        assert_eq!(district.center(), Vec3::new(150.0, 0.0, 125.0));
    }

    #[test]
    fn overlapping() {
        // Separate
        let a = Rect::new(0., 0., 1., 1.);
        let b = Rect::new(2., 2., 3., 3.);
        assert!(a.intersect(b).is_empty());

        // Overlapping corners
        let a = Rect::new(0., 0., 2., 2.);
        let b = Rect::new(1., 1., 3., 3.);
        assert!(a.intersect(b).is_empty().not());

        // Touching
        let a = Rect::new(0., 0., 2., 2.);
        let b = Rect::new(2., 0., 4., 2.);
        assert!(a.intersect(b).is_empty());

        let a = DistrictDescription {
            name: Name::new("a"),
            origin: Coordinates::new(
                Coordinate::<Longitude>::new(-60),
                Coordinate::<Latitude>::new(-49),
            ),
            variant: DistrictVariant {
                length: 30,
                width: 49,
                number: 0,
            },
            rotation: Rotation::CW270,
        };
        let b = DistrictDescription {
            name: Name::new("b"),
            origin: Coordinates::new(
                Coordinate::<Longitude>::new(-60),
                Coordinate::<Latitude>::new(-18),
            ),
            variant: DistrictVariant {
                length: 18,
                width: 30,
                number: 0,
            },
            rotation: Rotation::CW90,
        };

        assert!(a.overlaps(&b).not());
        assert!(b.overlaps(&a).not());
    }
}
