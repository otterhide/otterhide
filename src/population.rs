use crate::buildings::{
    Building, BuildingId, BuildingsSystems, HasSpareCapacity, IsOvercrowded, Residents,
};
use crate::date::{self, Date};
use crate::date::{NewMonth, SimulationFrameCounter};
use crate::day_month::{DayMonth, Duration};
use crate::history::Snapshot;
use crate::major_state::MajorState;
use crate::names::{FEMALE_NAMES, LAST_NAMES, MALE_NAMES};
use crate::simulation::SimulationParameters;
use bevy::ecs::system::SystemId;
use bevy::prelude::*;
use bevy::utils::{HashMap, Uuid};
use derive_more::{Display, From};
use itertools::Itertools;
use rand::distributions::Standard;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::ops::{Neg, Not};

pub struct PopulationPlugin;

impl Plugin for PopulationPlugin {
    fn build(&self, app: &mut App) {
        let aging_systems = (grow_up, grow_old);
        let reproduction_systems = (
            get_pregnant.run_if(in_state(MajorState::Simulating)),
            give_birth.run_if(in_state(MajorState::Simulating)),
            spawn_children.run_if(on_event::<ChildIsBorn>()),
        );

        app.init_resource::<PersonsRegister>()
            .register_type::<PersonsRegister>()
            .register_type::<Person>()
            .register_type::<PersonId>()
            .register_type::<Savings>()
            .register_type::<Sex>()
            .register_type::<PersonName>()
            .add_event::<Immigrated>()
            .add_event::<MovedIn>()
            .add_event::<MovedOut>()
            .add_event::<ChildIsBorn>()
            .add_event::<Died>()
            .add_systems(Startup, register_population_systems)
            .add_systems(
                Update,
                plan_immigration.before(implement_immigration).run_if(
                    in_state(MajorState::Simulating)
                        .and_then(resource_changed::<SimulationFrameCounter>)
                        .and_then(date::past_hour(6.0)),
                ),
            )
            .add_systems(
                Update,
                implement_immigration
                    .run_if(on_event::<Immigrated>())
                    .before(plan_moving_into_houses),
            )
            .add_systems(Update, aging_systems.run_if(on_event::<NewMonth>()))
            .add_systems(Update, reproduction_systems)
            .add_systems(PreUpdate, remove_dead)
            .add_systems(
                Update,
                plan_mortality
                    .before(mark_dead)
                    .after(move_into_houses)
                    .run_if(
                        in_state(MajorState::Simulating)
                            .and_then(resource_changed::<SimulationFrameCounter>)
                            .and_then(date::past_hour(6.0)),
                    ),
            )
            .add_systems(Update, mark_dead.run_if(on_event::<Died>()))
            .add_systems(
                Update,
                plan_moving_out_of_overcrowded_houses
                    .before(move_out_of_houses)
                    .run_if(
                        in_state(MajorState::Simulating)
                            .and_then(resource_changed::<SimulationFrameCounter>),
                    ),
            )
            .add_systems(Update, move_out_of_houses.run_if(on_event::<MovedOut>()))
            .add_systems(
                Update,
                plan_moving_into_houses.before(move_into_houses).run_if(
                    in_state(MajorState::Simulating)
                        .and_then(resource_changed::<SimulationFrameCounter>),
                ),
            )
            .add_systems(Update, move_into_houses.run_if(on_event::<MovedIn>()));
    }
}

// TODO: Add residency, so building can be updated too
#[derive(Event, Clone, Serialize, Deserialize, Debug)]
pub struct Died {
    pub person: PersonId,
}

#[derive(Component, Debug)]
pub struct Pregnant {
    father: PersonId,
    since: DayMonth,
}

#[derive(Event, Debug, Serialize, Deserialize, Clone)]
pub struct ChildIsBorn {
    pub child: PersonDescription,
}

const PREGNANCY_PROBABILITY: f64 = 0.01;
const PREGNANCY_DURATION: Duration = Duration::from_months(9.0);

fn get_pregnant(
    persons: Query<&Sex, (With<Adult>, Without<Senior>, Without<Pregnant>)>,
    buildings: Query<&Residents, With<Building>>,
    persons_register: Res<PersonsRegister>,
    date: Res<Date>,
    mut commands: Commands,
) {
    for residents in buildings.iter() {
        for partner_a_id in residents.0.iter() {
            if thread_rng().gen_bool(PREGNANCY_PROBABILITY).not() {
                continue;
            }
            let Some(partner_a_entity) = persons_register.0.get(partner_a_id) else {
                error!("Can't find entity for {partner_a_id:?} in the persons register.");
                continue;
            };
            let Ok(partner_a_sex) = persons.get(*partner_a_entity) else {
                // Not in reproductive age or already pregnant. Ignore.
                continue;
            };
            if *partner_a_sex != Sex::Female {
                continue;
            }
            let Some(partner_b_id) = residents.0.iter().choose(&mut thread_rng()) else {
                error!("Failed to select a random person from residents. Is it empty? Then the component should be removed.");
                continue;
            };
            let Some(partner_b_entity) = persons_register.0.get(partner_b_id) else {
                error!("Can't find entity for {partner_b_id:?} in the persons register.");
                continue;
            };
            let Ok(partner_b_sex) = persons.get(*partner_b_entity) else {
                // Not in reproductive age or already pregnant. Ignore.
                continue;
            };
            if *partner_b_sex != Sex::Male {
                continue;
            }

            debug!("{partner_a_id:?} got pregnant with {partner_b_id:?}");
            commands.entity(*partner_a_entity).insert(Pregnant {
                since: date.0,
                father: *partner_b_id,
            });
        }
    }
}

fn give_birth(
    pregnant_mothers: Query<(
        Entity,
        &PersonId,
        &Pregnant,
        Option<&Residence>,
        &PersonName,
    )>,
    fathers: Query<&PersonName, Without<Pregnant>>,
    date: Res<Date>,
    persons_register: Res<PersonsRegister>,
    mut child_birth: EventWriter<ChildIsBorn>,
    mut commands: Commands,
) {
    for (mother_entity, mother_id, pregnant, residence, mother_name) in pregnant_mothers.iter() {
        if &date.0 - &pregnant.since > PREGNANCY_DURATION {
            commands.entity(mother_entity).remove::<Pregnant>();

            // TODO: How to get fathers name after they are dead?
            let fathers_name: Option<&PersonName> = persons_register
                .0
                .get(&pregnant.father)
                .and_then(|father_entity| fathers.get(*father_entity).ok());

            let rng = &mut thread_rng();
            let id = Uuid::from_bytes(rng.gen()).into();
            let sex = rng.gen::<Sex>();
            let first_names = match sex {
                Sex::Male => MALE_NAMES,
                Sex::Female => FEMALE_NAMES,
            };
            let name = {
                PersonName {
                    first: first_names.choose(rng).unwrap().to_string(),
                    second: mother_name.second.clone(),
                    third: fathers_name.map(|name| name.second.clone()),
                }
            };
            let child = PersonDescription {
                id,
                name,
                sex,
                savings: Savings(0.0),
                residence: residence.map(|residence| residence.0),
                birth_date: date.0.into(),
            };
            child_birth.send(ChildIsBorn { child });
        }
    }
}

fn spawn_children(
    mut child_births: EventReader<ChildIsBorn>,
    systems: Res<PopulationSystems>,
    mut commands: Commands,
) {
    for ChildIsBorn { child } in child_births.read() {
        debug!("A child is born: {child:#?}");
        commands.run_system_with_input(systems.setup_new_person, child.clone());
    }
}

#[derive(Component, Debug)]
pub struct Adult;

#[derive(Component, Debug)]
pub struct Senior;

impl Adult {
    const AGE: f32 = 3.0;

    fn is_adult(date: &Date, birth_date: &BirthDate) -> bool {
        (&date.0 - &birth_date.0) > Duration::from_years(Self::AGE)
    }
}

impl Senior {
    const AGE: f32 = 8.0;

    fn is_senior(date: &Date, birth_date: &BirthDate) -> bool {
        (&date.0 - &birth_date.0) > Duration::from_years(Self::AGE)
    }
}

#[derive(Component, Debug)]
pub struct Dead;

/// Mark children coming of age as Adult
fn grow_up(
    children: Query<(Entity, &BirthDate), Without<Adult>>,
    date: Res<Date>,
    mut commands: Commands,
) {
    for (child, birth_date) in children.iter() {
        if Adult::is_adult(&date, birth_date) {
            commands.entity(child).insert(Adult);
        }
    }
}

/// Mark aging adults as Senior
fn grow_old(
    persons: Query<(Entity, &BirthDate), (With<Adult>, Without<Senior>)>,
    date: Res<Date>,
    mut commands: Commands,
) {
    for (person, birth_date) in persons.iter() {
        if Senior::is_senior(&date, &birth_date) {
            commands.entity(person).insert(Senior);
        };
    }
}

/// Select people to die
pub fn plan_mortality(
    persons: Query<(&PersonId, &BirthDate, Option<&Residence>)>,
    date: Res<Date>,
    simulation: Res<SimulationParameters>,
    mut died: EventWriter<Died>,
) {
    for (id, birth_date, residence) in persons.iter() {
        let age = Duration::new(&birth_date.0, &date.0).years();
        // TODO: Lower once we have reproduction system
        const EXTREMELY_OLD: u32 = 20 * 10;

        let numerator = age as u32 + 1;
        let denominator = EXTREMELY_OLD.max(numerator + 5) * simulation.frames_per_day_month as u32;
        if thread_rng().gen_ratio(numerator, denominator) {
            let residence = residence.map(|value| value.0.clone());
            debug!("Making {id:?} (residing at {residence:?}) dead");
            died.send(Died { person: id.clone() });
        };
    }
}

/// Make people who died as Dead
///
/// So they can be gracefully removed on the next frame
pub fn mark_dead(
    mut died: EventReader<Died>,
    mut commands: Commands,
    register: ResMut<PersonsRegister>,
) {
    for Died { person, .. } in died.read() {
        debug!("Marking the person {person:?} as dead");
        let entity = register.0.get(person).unwrap();
        commands.entity(*entity).insert(Dead);
    }
}

/// Deregister and despawn dead persons
pub fn remove_dead(
    dead: Query<(Entity, &PersonId, Option<&Residence>), With<Dead>>,
    mut register: ResMut<PersonsRegister>,
    building_systems: Res<BuildingsSystems>,
    mut commands: Commands,
) {
    for (entity, person, residence) in dead.iter() {
        debug!("Removing the dead person {person:?}");
        if let Some(Residence(building)) = residence {
            commands.run_system_with_input(
                building_systems.remove_resident,
                (building.clone(), person.clone()),
            )
        }
        register.0.remove(person);
        commands.entity(entity).despawn_recursive();
    }
}

#[derive(Resource, Clone, Default, Debug, Reflect)]
#[reflect(Resource)]
pub struct PersonsRegister(pub HashMap<PersonId, Entity>);

/// A logical description of a person suitable for events and snapshots
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PersonDescription {
    pub id: PersonId,
    pub name: PersonName,
    pub sex: Sex,
    pub savings: Savings,
    pub residence: Option<BuildingId>,
    pub birth_date: BirthDate,
    // TODO: Store information about each person's parents.
    // pub mother: Option<PersonId>,
    // pub father: Option<PersonId>,
}

/// A statistical distribution that holds the current date
///
/// so that randomly generated persons are not too old and not born in the
/// future.
struct PersonsDistribution {
    current_date: DayMonth,
}

impl From<Date> for PersonsDistribution {
    fn from(date: Date) -> Self {
        Self {
            current_date: date.0,
        }
    }
}

impl Distribution<PersonDescription> for PersonsDistribution {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> PersonDescription {
        let id = Uuid::from_bytes(rng.gen()).into();
        let sex = rng.gen::<Sex>();
        let first_names = match sex {
            Sex::Male => MALE_NAMES,
            Sex::Female => FEMALE_NAMES,
        };
        let name = {
            PersonName {
                first: first_names.choose(rng).unwrap().to_string(),
                second: LAST_NAMES.choose(rng).unwrap().to_string(),
                third: None,
            }
        };
        let age = Duration::from_months(rng.gen_range(1.0..120.0));
        let birth_date = self
            .current_date
            .clone()
            .advance(&age.neg())
            .to_owned()
            .into();
        let savings = rng.gen_range(100.0..10000.0).into();
        PersonDescription {
            id,
            name,
            savings,
            sex,
            birth_date,
            residence: None,
        }
    }
}

#[derive(Resource, Debug)]
pub struct PopulationSystems {
    pub setup_new_person: SystemId<PersonDescription>,
}

fn register_population_systems(world: &mut World) {
    let setup_new_person = world.register_system(setup_new_person);

    world.insert_resource(PopulationSystems { setup_new_person });
}

pub fn setup_new_person(
    In(person): In<PersonDescription>,
    mut commands: Commands,
    mut register: ResMut<PersonsRegister>,
    date: Res<Date>,
) {
    let id = person.id.clone();
    let residence = person.residence.clone();
    let birth_date = person.birth_date.clone();
    let mut entity = commands.spawn(PersonBundle::from(person));
    if let Some(building_id) = residence {
        entity.insert(Residence(building_id.clone()));
    };
    if Adult::is_adult(&date, &birth_date) {
        entity.insert(Adult);
    };
    if Senior::is_senior(&date, &birth_date) {
        entity.insert(Senior);
    };
    register.0.insert(id, entity.id());
}

#[derive(Event, Debug, Clone, From, Serialize, Deserialize)]
pub struct Immigrated(pub PersonDescription);

fn plan_immigration(mut immigrated: EventWriter<Immigrated>, date: Res<Date>) {
    let number = 20;
    debug!("{number} people immigrated to Otterhide.");

    let distribution = &PersonsDistribution::from(date.clone());
    for _index in 1..=number {
        let person: PersonDescription = distribution.sample(&mut thread_rng());
        immigrated.send(Immigrated::from(person));
    }
}

fn implement_immigration(
    mut immigrated: EventReader<Immigrated>,
    mut commands: Commands,
    systems: Res<PopulationSystems>,
) {
    for Immigrated(person) in immigrated.read().cloned() {
        debug!("New immigrant {person:#?} ");
        commands.run_system_with_input(systems.setup_new_person, person);
    }
}

#[derive(Event, Debug, Clone, Serialize, Deserialize)]
pub struct MovedIn {
    pub who: PersonId,
    pub where_to: BuildingId,
}

#[derive(Event, Debug, Clone, Serialize, Deserialize)]
pub struct MovedOut {
    pub who: PersonId,
    pub where_from: BuildingId,
}

fn plan_moving_into_houses(
    persons: Query<&PersonId, Without<Residence>>,
    buildings: Query<&BuildingId, With<HasSpareCapacity>>,
    mut moved_in: EventWriter<MovedIn>,
) {
    for person_id in persons.iter() {
        if random() {
            if let Some(building_id) = buildings.iter().choose(&mut thread_rng()) {
                moved_in.send(MovedIn {
                    who: person_id.clone(),
                    where_to: building_id.clone(),
                });
            }
        }
    }
}

fn move_into_houses(
    mut moved_in: EventReader<MovedIn>,
    persons_register: Res<PersonsRegister>,
    mut commands: Commands,
) {
    for event in moved_in.read() {
        debug!("Moving into a house {event:#?}");

        let MovedIn { who, where_to } = event;
        let Some(person) = persons_register.0.get(who) else {
            // TODO: Don't panic. Make it so, that all events can fail without leaving the world in inconsitent state.
            panic!("Person {who:#?} not in the register! {persons_register:#?}");
        };
        commands.entity(*person).insert(Residence(where_to.clone()));
    }
}

fn plan_moving_out_of_overcrowded_houses(
    buildings: Query<(&BuildingId, &Residents), With<IsOvercrowded>>,
    mut moved_out: EventWriter<MovedOut>,
) {
    for (building, residents) in buildings.iter() {
        let probability = residents.0.len() as f64 / 100.0;
        for resident in residents.0.iter() {
            if thread_rng().gen_bool(probability.min(0.5)) {
                moved_out.send(MovedOut {
                    who: resident.clone(),
                    where_from: building.clone(),
                });
            }
        }
    }
}

fn move_out_of_houses(
    mut moved_out: EventReader<MovedOut>,
    persons_register: Res<PersonsRegister>,
    mut commands: Commands,
) {
    for event in moved_out.read() {
        debug!("Moving out of a house {event:#?}");

        let MovedOut { who, where_from: _ } = event;
        let Some(person) = persons_register.0.get(who) else {
            warn!(
                "Person {who:#?} not in the register! {persons_register:#?}. Maybe they are dead?"
            );
            return;
        };
        commands.entity(*person).remove::<Residence>();
    }
}

#[derive(Bundle)]
pub struct PersonBundle {
    pub id: PersonId,
    pub name: PersonName,
    pub sex: Sex,
    pub savings: Savings,
    pub birth_date: BirthDate,

    // Derived from the above
    pub display_name: Name,
    pub marker: Person,
}

impl PersonBundle {
    pub fn new(
        id: PersonId,
        name: PersonName,
        sex: Sex,
        birth_date: BirthDate,
        savings: Savings,
    ) -> Self {
        let display_name = name.to_string().into();
        Self {
            id,
            name,
            sex,
            birth_date,
            savings,
            marker: Person,
            display_name,
        }
    }
}

impl From<PersonDescription> for PersonBundle {
    fn from(value: PersonDescription) -> Self {
        Self::new(
            value.id,
            value.name,
            value.sex,
            value.birth_date,
            value.savings,
        )
    }
}

#[derive(Component, Debug, Reflect)]
pub struct Person;

#[derive(
    Component,
    Debug,
    Clone,
    Copy,
    Hash,
    PartialEq,
    Eq,
    Reflect,
    From,
    Display,
    Serialize,
    Deserialize,
)]
pub struct PersonId(pub Uuid);

#[derive(Component, Reflect, Clone, Debug, From, Serialize, Deserialize)]
pub struct Savings(pub f32);

#[derive(Component, Clone, Debug, From, Serialize, Deserialize)]
pub struct BirthDate(pub DayMonth);

#[derive(Component, Reflect, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub enum Sex {
    Male,
    Female,
}

impl Distribution<Sex> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Sex {
        if rng.gen_bool(0.5) {
            Sex::Male
        } else {
            Sex::Female
        }
    }
}

impl Display for Sex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Sex::Male => write!(f, "♂"),
            Sex::Female => write!(f, "♀"),
        }
    }
}

#[derive(Component, Reflect, Debug, Clone, Serialize, Deserialize)]
pub struct PersonName {
    first: String,
    second: String,
    third: Option<String>,
}

impl Display for PersonName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let PersonName {
            first,
            second,
            third,
        } = self;

        match third {
            None => write!(f, "{first} de {second}"),
            Some(third) => write!(f, "{first} de {second}-{third}"),
        }
    }
}

#[derive(Component, Debug, Clone)]
pub struct Residence(pub BuildingId);

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PopulationSnapshot {
    pub persons: Vec<PersonDescription>,
}

impl Snapshot for PopulationSnapshot {
    fn capture(world: &mut World) -> Self {
        let mut query = world.query_filtered::<(
            &PersonId,
            &PersonName,
            &Sex,
            &BirthDate,
            &Savings,
            Option<&Residence>,
        ), Without<Dead>>();
        // TODO: Can I satisfy the borrow checker without collecting the iterator?
        let persons = query
            .iter(world)
            .map(
                |(id, name, sex, birth_date, savings, residence)| PersonDescription {
                    id: id.clone(),
                    name: name.clone(),
                    sex: sex.clone(),
                    birth_date: birth_date.clone(),
                    savings: savings.clone(),
                    residence: residence.map(|Residence(building_id)| building_id.clone()),
                },
            )
            .collect_vec();

        Self { persons }
    }

    fn restore(&self, world: &mut World) {
        let mut query = world.query_filtered::<Entity, With<Person>>();
        // TODO: Can I satisfy the borrow checker without collecting the iterator?
        let persons = query.iter(world).collect_vec();
        for person in persons {
            world.despawn(person);
        }

        world.resource_scope(|_, mut register: Mut<PersonsRegister>| register.0.clear());

        for person in self.persons.iter() {
            world.resource_scope(|world, systems: Mut<PopulationSystems>| {
                debug!("Restoring {person:#?}");
                world
                    .run_system_with_input(systems.setup_new_person, person.clone())
                    .unwrap()
            });
        }
    }
}
