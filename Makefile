include Makefile.d/defaults.mk

all: ## Build the program (DEFAULT)
all: test build
.PHONY: all

build: $(shell find src/)
build: Cargo.lock
build:
	cargo build --release
.PHONY: build

test:
	cargo test --features bevy/dynamic_linking
.PHONY: test

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: build
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive $</* $(prefix)/
.PHONY: install


### DEVELOPMENT

develop: ## Rebuild and run a development version of the program
develop: agrs := ""
develop:
	cargo run --features bevy/dynamic_linking --bin otterhide -- $(args)
.PHONY: develop

clean: ## Remove all build artifacts
clean:
	git clean -dfX \
		--exclude='!.envrc.private'
.PHONY: clean

check: ## Check the code
check:
	cargo test
	cargo clippy
.PHONY: check

check/watch: ## Keep checking while watching for changes
check/watch:
	watchexec \
	--watch src/ \
	--debounce 1s \
	'make check'
.PHONY: check/watch

check/sqlite-export: ## Try exporting the simulation.ron file to SQL
check/sqlite-export: data/sqlite-export.sql
.PHONY: check/sqlite-export

check/sqlite-export/watch: ## Watch for code changes and re-try SQLite export
check/sqlite-export/watch:
	watchexec \
	--watch src/ \
	--debounce 1s \
	'make check/sqlite-export --assume-new simulation.ron'
.PHONY: check/sqlite-export/watch

check/sqlite-export/schema: ## Check if the SQLite schema file is valid
check/sqlite-export/schema: src/sqlite-export-schema.sql
	sqlite3 \
	--init $< \
	--bail \
	data/sqlite-export.db \
	.dump
.PHONY: check/sqlite-export/schema

check/sqlite-export/schema/watch: ## Keep checking the schema file while it changes
check/sqlite-export/schema/watch:
	watchexec \
	--watch src/ \
	--debounce 1s \
	'make check/sqlite-export/schema'
.PHONY: check/sqlite-export/schema/watch


data/sqlite-export.sql: data/sqlite-export.db
	mkdir --parents $(@D)
	sqlite3 $< .dump > $@

data/sqlite-export.db: simulation.ron
data/sqlite-export.db: check/sqlite-export/schema
data/sqlite-export.db: src/**/*
data/sqlite-export.db:
	mkdir --parents $(@D)
	cargo run --bin otterhide-to-sqlite -- --failed-database=data/failed.db $< $@


help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

### WEB TARGET

web: ## Build the program for the web
web: $(shell find src/)
web: $(shell find web-assets/)
web: Cargo.lock
web: index.html
web: base_url ?= "/"
web:
	trunk build \
		--release \
		--public-url $(base_url) \
		--dist $@
	touch $@

web/develop: ## Rebuild and serve a development version of the in a local server
web/develop:
	trunk serve \
		--address=0.0.0.0 \
		--no-spa \
		index.html
.PHONY: web/develop

web/serve: ## Serve a release version of the program
web/serve: web
web/serve:
	miniserve --index=index.html $<
.PHONY: web/serve


### CONTINUOUS INTEGRATION

public: ## Prepare the web assets (used in CI)
public: web
public:
	rm --recursive --force $@
	cp --recursive --force $^ $@
	unset GZIP
	find $@ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\|wasm\|ron\)$$' -exec gzip -9 -f -k {} \;
	find $@ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\|wasm\|ron\)$$' -exec brotli --force --keep {} \;
	touch $@
.PHONY: public


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)


### OCI BUILD IMAGE RELATED

build-image: ## Create an OCI image with build environment (to be used in CI)
build-image: flake.nix
build-image: flake.lock
build-image: rust-toolchain.toml
build-image:
	nix build \
		--print-build-logs \
		.#docker-image
	cp --dereference result $@

load-build-image: ## Load the OCI image using Podman
load-build-image: build-image
	gzip --decompress $^ --to-stdout | podman image load
.PHONY: load-build-image

build-inside-container: ## Build the program in an OCI container (e.g. to test CI)
build-inside-container: load-build-image
	# We need to know the project name to know which image to load. Can we do better?
	test $${project_name}
	podman run \
		--rm \
		--interactive \
		--tty \
		--volume ${PWD}:/src \
		--workdir /src \
		--entrypoint make \
		localhost/$(project_name)-build-image \
		public
.PHONY: build-inside-container
