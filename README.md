-   Run Bevy locally after setting up nix:
    -   `nix develop`
    -   `make web/develop` (default runs 8080)


# Example queries

Who lived where:

``` sql
select 
    name,
    address,
    concat (from_year, '-', from_month) as moved_in,
    concat (until_year, '-', until_month) as moved_out
from 
    residency
    inner join person on residency.person = person.id 
    inner join building on residency.building = building.id 
;
```
